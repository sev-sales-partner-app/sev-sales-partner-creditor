import { netToGross } from '../util'
import {
  DebitorAccount,
  EntitlementAccount,
  IncomeAccount
} from '../util/constants'
// import { p } from '../util/debug'
import { defaultTo, match, pipe } from '../util/fp'
import { furyTo$, $toFury } from '../util/furyNumbers'

// fromOrSender :: Object -> String
export function fromOrSender (tx = {}) {
  return tx.from ? 'from' : 'sender'
}

// toOrRecipient :: Object -> String
export function toOrRecipient (tx = {}) {
  return tx.to ? 'to' : 'recipient'
}

// isInvoic :: String -> Boolean
export function isInvoic (data) { return Number.parseInt(data, 16) === 0 }

// isPayableFor :: String -> Object -> Boolean
export function isPayableFor (accountAddress) {
  return function (tx = {}) {
    const fromOrSenderTx = fromOrSender(tx)
    if (tx[fromOrSenderTx] == null) {
      return false
    } else if ('data' in tx) {
      return !isInvoic(tx.data)
        ? tx[fromOrSenderTx].toLowerCase() === accountAddress.toLowerCase()
        : !(tx[fromOrSenderTx].toLowerCase() === accountAddress.toLowerCase())
    } else {
      return tx[fromOrSenderTx].toLowerCase() === accountAddress.toLowerCase()
    }
  }
}

// selectFrom -> Object -> String -> a
function selectFrom (dataByKey) {
  return function (key) { return dataByKey[key] }
}

// calcBalance :: Object -> Number
export function calcBalance ({ credit, debit }) { // save
  return [[ debit != null, credit != null ]]
    .filter(conditions => conditions.every(Boolean))
    .flatMap(() => defaultTo(0)(credit.value - debit.value))[0] || 0
}

// calcVBalance :: Object -> Number
export function calcVBalance ({ address, txs, rxs }) { // save
  const xs = txs || rxs || []
  return [[ Array.isArray(xs), typeof address === 'string' ]]
    .filter(conditions => conditions.every(Boolean))
    .map(() =>
      xs.reduce((acc, x) => {
        isPayableFor(address)(x)
          ? acc -= Number.parseInt(x.value, 16)
          : acc += Number.parseInt(x.value, 16)
        return acc
      }, 0)
    )[0] || 0
}

// filterTxs :: (String, { address: String, txs: Array | rxs: Array }) -> Array
export function filterTxs (filter, { address, txs, rxs }) { // save
  // a -> b -> Boolean
  const equals = p => v => v === p
  const xs = txs || rxs || []
  return [[ Array.isArray(xs) ]]
    .filter(conditions => conditions.every(Boolean))
    .flatMap(() => match(filter)
      .on(equals('sent'),
        () => xs.filter(x => x[fromOrSender(x)].toLowerCase() === address.toLowerCase()))
      .on(equals('received'),
        () => xs.filter(x => x[fromOrSender(x)].toLowerCase() !== address.toLowerCase()))
      .otherwise(() => xs))
}

// computeCommission :: Object -> Object
export function computeCommission (accountsByRef) {
  const { round } = Math

  // $balance -> String -> Number
  const $balance = pipe(
    selectFrom(accountsByRef),
    defaultTo({}),
    calcBalance,
    furyTo$,
    $toFury
  )

  const entitlement = $balance(EntitlementAccount)
  const debitorPayable = $balance(DebitorAccount)
  const debitorCleared = $balance(IncomeAccount) // [debitor.ledger][user.address]
  const entitlementTotal = round(netToGross(entitlement))
  // const entitlementVat = round(calcVatValue(entitlement)) // todo redundant
  const debitorBalance = debitorCleared + debitorPayable
  const receivableTotal = debitorBalance * -1
  // const receivable = round(grossToNet(receivableTotal)) // todo redundant
  // const receivableVat = receivableTotal - receivable // todo redundant
  // const settleable = entitlement - receivable // todo redundant
  const settleableTotal = entitlementTotal + debitorBalance // or minus receivableTotal
  // const settleableVat = settleableTotal - settleable // todo redundant

  return {
    entitlement,
    // entitlementVat,
    entitlementTotal,
    // receivable,
    // receivableVat,
    debitorBalance,
    receivableTotal,
    // settleable,
    // settleableVat,
    settleableTotal
  }
}

// export function computeAccount ({ pageId, params, accountRef, accounts }) {
//   const fallback = IncomeAccount
//   const ref = match({ params, id: pageId })
//     .on(x => accountRef[x.id] != null, (x) => x.id)
//     .on(x => x.id === SettlementPage, () => SettlementsAccount)
//     .otherwise(() => fallback)
//
//   const selector = accountRef[ref] != null ? accountRef[ref] : accountRef[fallback]
//   const selected = accounts[selector.ledger] && accounts[selector.ledger][selector.address]
//
//   // account with selector keys
//   return Object.assign({}, selector, { ref }, selected != null ? selected : {})
// }
//
// export function computeFn (state) {
//   const {
//     accounts,
//     debitor,
//     params,
//     pageId,
//     settlement,
//     user,
//     ctx,
//     auth
//   } = state
//
//   const { accountRef } = ctx
//
//   ctx['commission'] = computeCommission({ accounts, ctx })
//   const account = computeAccount({ pageId, params, accountRef, accounts })
//
//   // computed State
//   return { account, auth, ctx, debitor, pageId, params, settlement, user }
// }
