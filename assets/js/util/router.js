// import { assoc } from './fp'
import Mapper from 'url-mapper'

// pageIds
export const UserAccount = 'p-user'
export const EntitlementAccount = 'p-entitlement'
export const SettlementsAccount = 'p-settlements'
export const DebitorAccount = 'p-debitor'
export const QueriedAccount = 'p-queried'
export const IncomeAccount = 'p-income'

// routing
export const HomePage = 'p-home'
export const SignInPage = 'p-signIn'
export const AccountPage = 'p-account'
export const SettlementPage = 'p-settlement'

export const prefix = '#'
export const defaultRoute = '/'

const routeMappings = {
  '/': () => ({ pageId: HomePage }),
  '/signin': () => ({ pageId: SignInPage }),
  '/settlement/:msgId?': () => ({ pageId: SettlementPage }),
  '/debitor/:filter?': () => ({ pageId: DebitorAccount }),
  '/entitlement/:filter?': () => ({ pageId: EntitlementAccount }),
  '/income/:filter?': () => ({ pageId: IncomeAccount }),
  '/queried': () => ({ pageId: QueriedAccount }),
  '/settlements/:filter?': () => ({ pageId: SettlementsAccount }),
  '/user': () => ({ pageId: UserAccount })
}

const urlMapper = Mapper({ query: true })

const routeLookup = Object.keys(routeMappings).reduce((acc, key) => {
  acc[routeMappings[key]().pageId] = key; return acc
  // return assoc(routeMappings[key]().pageId)(key)(acc)
}, {})

// parseUrl :: String -> Object
export const parseUrl = (url = window.location.hash || prefix + defaultRoute) => { // '#/'
  const mapped = urlMapper.map(url.substring(prefix.length), routeMappings)
  if (mapped) {
    const patch = mapped.match // { pageId }
    return Object.assign({}, patch(), { url, params: mapped.values })
  } else {
    return { pageId: undefined, url, params: {} }
  }
}

// getUrl :: (String, Object) -> String
export const getUrl = (id, params = {}) => {
  const route = routeLookup[id] || defaultRoute // '/'
  const result = urlMapper.stringify(route, params)
  return prefix + result
}

// navigateTo :: (String, Object) -> Object
export const navigateTo = (id, params) => parseUrl(getUrl(id, params))

// navigateTo :: Function -> void 0
export const listenToRouteChanges = update => {
  // const { pageId, params, url, ...rest } = parseUrl()
  window.onpopstate = () => update(model => Object.assign({}, model, parseUrl()))
  console.info('[listenToRouteChanges :: router]',
    'router is initialized and listens for \'window.onpopstate\' events...')
}
