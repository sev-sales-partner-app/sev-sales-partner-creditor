// getFn :: (Object, String) -> Function
function getFn (component, prop) {
  return component[prop] || (() => null)
}

// wireView :: (Object, Function) -> Function
function wireView (component, update, getState, parentActions = {}) {
  const dependencies = {}
  const actions = getFn(component, 'actions')({ update, getState, parentActions })
  Object.keys(component.dependencies || {}).forEach(key => {
    dependencies[key] = wireView(component.dependencies[key], update, getState, actions)
  })
  return getFn(component, 'view')(Object.assign({ actions }, dependencies))
}

// wireView :: (Object, Array) -> Array
function getDependencies (component, dependencies = []) {
  Object.values(component.dependencies || {}).forEach(dependency => {
    if (dependencies.indexOf(dependency) < 0) {
      dependencies.push(dependency)
    }
    getDependencies(dependency, dependencies)
  })
  return dependencies
}

// wirem :: (Object, Function, Object, Object) -> Object
export function wirem ({ component, update, getState, properties, combinators }) {
  const view = wireView(component, update, getState)
  const components = [component].concat(getDependencies(component))
  const aggregates = Object.keys(combinators || {}).reduce((result, key) => {
    result[key] = combinators[key](components.map(dependency => dependency[key]).filter(x => x))
    return result
  }, {})
  return Object.assign({}, properties, aggregates, { view })
}
