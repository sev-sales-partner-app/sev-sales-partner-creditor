import React from 'react'
import ReactDOM from 'react-dom'
import { sv } from 'seview'
import b from 'bss/bss.js'
import { createBssHelpers } from 'stylething/bssHelpers.esm'
import * as theme from 'stylething/theme.esm'

// seview-react set up
const { createElement } = React

const h = sv(node => {
  if (typeof node === 'string') {
    return node
  }
  const attrs = node.attrs || {}
  if (attrs.innerHTML) {
    attrs.dangerouslySetInnerHTML = { __html: attrs.innerHTML }
    delete attrs.innerHTML
  }
  const args = [node.tag, node.attrs || {}].concat(node.children || [])
  return createElement.apply(null, args)
})

const render = element => view => ReactDOM.render(h(view), element)

// bss helper initialisation
const { helper } = b
const preset = createBssHelpers(b)
helper(preset)

export { b, createElement, render, preset, theme }
