import t from 'ramda/es/tap'

// Credit: https://medium.com/@jperasmus11/roll-your-own-async-compose-pipe-functions-658cafe4c46f
// const asyncPipe = (...fns) => input => fns.reduce((chain, fn) => chain.then(fn), Promise.resolve(input))
export function asyncPipe (...fns) {
  return function (input) { return fns.reduce((acc, fn) => acc.then(fn), Promise.resolve(input)) }
}

// Using reduce, courtesy Barney Carroll (https://github.com/barneycarroll)
export function get (object, path) {
  return path.reduce((obj, key) => obj == null ? undefined : obj[key], object)
}

export function set (object, path, value) {
  const head = path[0]
  if (path.length === 1) {
    object[head] = value
  } else {
    if (object[head] === undefined) {
      object[head] = {}
    }
    set(object[head], path.slice(1), value)
  }
  return object
}

export function updateWith (obj, path, fn) {
  return set(obj, path, fn(get(obj, path)))
}

export const preventDefault = t(e => e.preventDefault())

// https://codeburst.io/alternative-to-javascripts-switch-statement-with-a-functional-twist-3f572787ba1c
const matched = x => ({
  on: () => matched(x),
  otherwise: () => x
})
export const match = x => ({
  on: (pred, fn) => (pred(x) ? matched(fn(x)) : match(x)),
  otherwise: fn => fn(x)
})

/**
 *
 * Function forwarding
 */

// assoc :: String → a → {k: v} → {k: v}
export { default as assoc } from 'ramda/es/assoc'

// assocPath :: [Idx] → a → {a} → {a}
// Idx = String | Int
export { default as assocPath } from 'ramda/es/assocPath'

// dissoc :: String → {k: v} → {k: v}
export { default as dissoc } from 'ramda/es/dissoc'

// dissocPath :: [Idx] → {k: v} → {k: v}
// Idx = String | Int
export { default as dissocPath } from 'ramda/es/dissocPath'

// [a] → a | Undefined
// String → String
export { default as head } from 'ramda/es/head'

// identity :: a → a
export { default as identity } from 'ramda/es/identity'

// a → Boolean
export { default as isEmpty } from 'ramda/es/isEmpty'

// merge :: {k: v} → {k: v} → {k: v} // TODO rename to mergeRight
export { default as merge } from 'ramda/es/mergeRight'

// mergeDeepLeft :: {a} → {a} → {a}
export { default as mergeDeepLeft } from 'ramda/es/mergeDeepLeft'

// mergeDeepRight :: {a} → {a} → {a}
export { default as mergeDeepRight } from 'ramda/es/mergeDeepRight'

// prop :: s → {s: a} → a | Undefined
export { default as prop } from 'ramda/es/prop'

// String → a → Object → Boolean
export { default as propEq } from 'ramda/es/propEq'

// compose :: ((y → z), (x → y), …, (o → p), ((a, b, …, n) → o)) → ((a, b, …, n) → z)
// https://medium.com/javascript-scene/master-the-javascript-interview-what-is-function-composition-20dfb109a1a0
export { default as compose } from 'ramda/es/compose'

// constant :: a → (* → a)
export { default as constant } from 'ramda/es/always'

// defaultTo :: a → b → a | b
export { default as defaultTo } from 'ramda/es/defaultTo'

// omit :: [String] → {String: *} → {String: *}
export { default as omit } from 'ramda/es/omit'

// path :: [Idx] → {a} → a | Undefined
// Idx = String | Int
export { default as path } from 'ramda/es/path'

// pick :: [k] → {k: v} → {k: v}
export { default as pick } from 'ramda/es/pick'

// pipe :: (((a, b, …, n) → o), (o → p), …, (x → y), (y → z)) → ((a, b, …, n) → z)
export { default as pipe } from 'ramda/es/pipe'

// tap :: (a → *) → a → a
export { default as tap } from 'ramda/es/tap'

// thrush | applyTo :: a → (a → b) → b
export { default as thrush } from 'ramda/es/applyTo'

// unnest :: Chain c => c (c a) → c a
export { default as unnest } from 'ramda/es/unnest'

/**
 *
 * // Combinators
 * // Reference: https://gist.github.com/Avaq/1f0636ec5c8d6aed2e45
 *
 * // combinators.js
 *
 * const I = x => x;
 * const K = x => y => x;
 * const A = f => x => f(x);
 * const T = x => f => f(x);
 * const W = f => x => f(x)(x);
 * const C = f => y => x => f(x)(y);
 * const B = f => g => x => f(g(x));
 * const S = f => g => x => f(x)(g(x));
 * const P = f => g => x => y => f(g(x))(g(y));
 * const Y = f => (g => g(g))(g => f(x => g(g)(x)));
 *
 *
 * Name         | #     | [Haskell][]    | [Ramda][]  | [Sanctuary][]     | Signature
 * ------------:|-------|----------------|------------|-------------------|----------
 * identity     | **I** | `id`           | `identity` | `I`               | `a → a`
 * constant     | **K** | `const`        | `always`   | `K`               | `a → b → a`
 * apply¹       | **A** | `($)`          | `call`     |                   | `(a → b) → a → b`
 * thrush       | **T** | `(&)`          | `applyTo`  | `T`               | `a → (a → b) → b`
 * duplication  | **W** | `join`²        | `unnest`²  | `join`²           | `(a → a → b) → a → b`
 * flip         | **C** | `flip`         | `flip`     | `flip`            | `(a → b → c) → b → a → c`
 * compose      | **B** | `(.)`, `fmap`² | `map`²     | `compose`, `map`² | `(b → c) → (a → b) → a → c`
 * substitution | **S** | `ap`²          | `ap`²      | `ap`²             | `(a → b → c) → (a → b) → a → c`
 * psi          | **P** | `on`           |            | `on`              | `(b → b → c) → (a → b) → a → a → c`
 * fix-point³   | **Y** | `fix`          |            |                   | `(a → a) → a`
 *
 * -----
 * ¹) The A-combinator can be implemented as an alias of the I-combinator.
 * Its implementation in Haskell exists because the infix nature gives it
 * some utility. Its implementation in Ramda exists because it is
 * overloaded with additional functionality.
 *
 * ²) Algebras like `ap` have different implementations for different types.
 * They work like Function combinators only for Function inputs.
 *
 * ³) In JavaScript and other non-lazy languages, it is impossible to implement the
 * Y-combinator. Instead a variant known as the *applicative* or *strict*
 * fix-point combinator is implemented. This variant is sometimes referred to as
 * the Z-combinator.
 *
 * [Haskell]: https://www.haskell.org/
 * [Ramda]: http://ramdajs.com/
 * [Sanctuary]: http://sanctuary.js.org/#combinator
 */

/*
// reference: https://github.com/foxdonut/meiosis-examples/blob/master/examples/realworld/src/util/fp.js

import { compose, constant, defaultTo, omit, path, pick, pipe, tap, thrush } from "tinyfunk"

// Using reduce, courtesy Barney Carroll (https://github.com/barneycarroll)
const get = (object, path) =>
  path.reduce((obj, key) => obj == undefined ? undefined : obj[key], object)

// Credit: https://medium.com/@jperasmus11/roll-your-own-async-compose-pipe-functions-658cafe4c46f
const asyncPipe = (...fns) => input =>
  fns.reduce((chain, fn) => chain.then(fn), Promise.resolve(input))

module.exports = {
  asyncPipe,
  compose,
  constant,
  defaultTo,
  get,
  omit,
  path,
  pick,
  pipe,
  preventDefault: tap(evt => evt.preventDefault()),
  tap,
  thrush
}
*/
