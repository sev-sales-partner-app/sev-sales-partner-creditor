// pageIds
export {
  UserAccount,
  EntitlementAccount,
  SettlementsAccount,
  DebitorAccount,
  QueriedAccount,
  IncomeAccount,
  HomePage,
  SignInPage,
  AccountPage,
  SettlementPage,
  prefix,
  defaultRoute
} from './router'

export const blankHref = 'javascript://'

// addresses
export const OxOO = '0x0000000000000000000000000000000000000000'
export const settlementLedger = '0x5856b2AE31ed0FCf82F02a4090502DC5CCEec93E'

// Hosts
export const apiHost =
  process.env.NODE_ENV === 'production'
    ? 'https://mad-meek-meteor.nanoapp.io/api'
    : 'http://sev.local:3000/api'
export const rpcHost = 'https://fury.network/rpc' // 'https://demo.stromdao.de/rpc'
export const abiLocation = '/vendor/smart_contractsV0554' // 'https://unpkg.com/stromdao-businessobject@0.5.54/smart_contracts'
