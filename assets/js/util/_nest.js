import { get, set, updateWith } from './fp'

// https://github.com/foxdonut/meiosis/blob/master/docs/lessons/99-Appendix-A-Pattern-Code-Snippets.md#get-and-set-functions

export const nestUpdate = (update, path) => fn =>
  update(fn.ctx ? fn : model => updateWith(model, path, fn))

export const nest = function (create, path, options = {}) {
  const component = create(options.update
    ? Object.assign({}, options, { update: nestUpdate(options.update, path) })
    : options
  )
  const result = Object.assign({}, component)

  if (component.model) {
    result.model = () => set({}, path, component.model())
  }

  if (component.view) {
    result.view = model => component.view(
      Object.assign({ ctx: model.ctx }, get(model, path)))
  }
  return result
}

/*
// using Patchinko
// https://github.com/foxdonut/meiosis-examples/blob/master/examples/realworld/src/util/nest.js

import O from "patchinko/constant"

export const nestPatch = (object, path) => ({
[path[0]]: path.length === 1
  ? O(object)
  : O(nestPatch(object, path.slice(1)))
})

export const nestUpdate = (update, path) => patch =>
  update(patch.ctx ? patch : nestPatch(patch, path))

export const nest = (create, path, options) => {
  const component = create(O(options, { update: nestUpdate(options.update, path) }))
  const result = O({}, component)

  if (component.model) {
    result.model = () => nestPatch(component.model(), path)
  }

  if (component.view) {
    result.view = model => component.view(O({ ctx: model.ctx }, get(model, path)))
  }

  return result
}
*/
