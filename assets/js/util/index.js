export const now = () => Math.round(new Date().getTime() / 1000)

// VAT
export const calcVatValue = (net, rate = 0.19) => net * rate
export const netToGross = (net, rate = 0.19) => net + calcVatValue(net, rate)
export const grossToNet = (gross, rate = 0.19) => gross / (rate + 1)

// https://gist.github.com/pirate/9298155edda679510723
export const decodeURLParams = search => {
  if (search === '') return {}
  const hashes = search.slice(search.indexOf('?') + 1).split('&')
  return hashes.reduce((params, hash) => {
    const split = hash.indexOf('=')
    const key = hash.slice(0, split)
    const val = hash.slice(split + 1)
    return Object.assign(params, key ? { [key]: decodeURIComponent(val) } : {})
  }, {})
}

export const isOdd = num => num % 2
export { preventDefault } from './fp'

// https://stackoverflow.com/questions/1634748/how-can-i-delete-a-query-string-parameter-in-javascript
export function removeUrlParameter (url, parameter) {
  const urlParts = url.split('?')

  if (urlParts.length >= 2) {
    // Get first part, and remove from array
    const urlBase = urlParts.shift()
    // Join it back up
    const queryString = urlParts.join('?')
    const prefix = encodeURIComponent(parameter) + '='
    const parts = queryString.split(/[&;]/g)
    // Reverse iteration as may be destructive
    for (let i = parts.length; i-- > 0;) {
      // Idiom for string.startsWith
      if (parts[i].lastIndexOf(prefix, 0) !== -1) {
        parts.splice(i, 1)
      }
    }
    url = urlBase + (parts.length > 0 ? '?' + parts.join('&') : '')
  }
  return url
}

// // helpers AES encryption
// export const encrypt = (data, salt) => {
//   let cipher = crypto.createCipher('aes-256-ctr', salt)
//   let crypted = cipher.update(data, 'utf8', 'hex')
//   crypted += cipher.final('hex')
//   return crypted
// }
//
// export const decrypt = (data, salt) => {
//   let decipher = crypto.createDecipher('aes-256-ctr', salt)
//   let dec = decipher.update(data, 'hex', 'utf8')
//   dec += decipher.final('utf8')
//   return dec
// }
