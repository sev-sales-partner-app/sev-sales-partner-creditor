// import { p } from '../util/debug'
import {
  assocPath,
  compose,
  preventDefault
} from '../util/fp'

import { b as css, theme } from '../util/view'

import {
  validateSalesPartnerId,
  validateUsername,
  validateCredentials
} from '../validation/credentials'

import {
  Input,
  Label,
  Hint
} from '../common'

const createActions = ({ update, parentActions }) => ({

  setUsername: (id, err) => ev => {
    const value = ev.target.value
    const username = err ? validateUsername(value) : { value, error: err }
    update(assocPath([ id, 'username' ])(username))
  },

  setSalesPartnerId: (id, err) => ev => {
    const value = ev.target.value
    const salesPartnerId = err ? validateSalesPartnerId(value) : { value, error: err }
    update(assocPath([ id, 'salesPartnerId' ])(salesPartnerId))
  },

  handleFormData: id => ev => {
    const form = ev.target
    const username = form.elements.username.value
    const salesPartnerId = form.elements.password.value
    const callbackUrl = form.elements.callbackUrl.value
    const validatedCreds = validateCredentials(username, salesPartnerId)
    const errs = Object.values(validatedCreds).map(v => v.error).filter(err => err !== '')

    update(compose(
      assocPath([ id, 'username' ])(validatedCreds.username),
      assocPath([ id, 'salesPartnerId' ])(validatedCreds.salesPartnerId)
    ))

    errs.length && console.error('[handleFormData :: action]', 'validation errors:', errs)
    errs.length === 0 &&
    parentActions.submitSignInRequest(validatedCreds, callbackUrl)
  }
})

const createView = options => ({ actions }) => {
  const id = options.id || 'credentials'

  return function CredentialsForm (state) {
    const { username, salesPartnerId } = state[id]
    const callbackUrl = decodeURI(window.location.href)
    const { space } = theme
    const wrapper = 'div' + css`margin-top: ${space[3]}`

    return [
      'form', {
        noValidate: true,
        onSubmit: compose(
          actions.handleFormData(id),
          preventDefault
        )
      }, [

        [wrapper, { htmlFor: 'username' },
          [Label, { htmlFor: 'username' }, 'User Name'],
          [Input, {
            name: 'username',
            type: 'email',
            placeholder: 'beispiel@email.de',
            required: true,
            error: username.error,
            onChange: actions.setUsername(id, username.error)
          }],
          username.error && [Hint, { error: username.error }]
        ],

        [wrapper, { htmlFor: 'password' },
          [Label, { htmlFor: 'password' }, 'Sales Partner Id'],
          [Input, {
            name: 'password',
            type: 'text',
            placeholder: 'c3ec23a16304f8d6c8692dcac2343c05',
            required: true,
            error: salesPartnerId.error,
            onChange: actions.setSalesPartnerId(id, salesPartnerId.error)
          }],
          salesPartnerId.error && [Hint, { error: salesPartnerId.error }]
        ],

        ['input', { type: 'text', name: 'callbackUrl', hidden: true, defaultValue: callbackUrl }],

        [wrapper,
          [Input, { type: 'submit', value: 'Sign in' }]]
      ]
    ]
  }
}

export const createCredentials = options => ({ parentActions, update }) => {
  const actions = createActions({ update, parentActions })
  return {
    model: () => ({
      username: { value: '', error: '' },
      salesPartnerId: { value: '', error: '' }
    }),
    view: createView(options)({ actions })
  }
}

export const Credentials = options => ({
  actions: createActions,
  model: () => ({
    username: { value: '', error: '' },
    salesPartnerId: { value: '', error: '' }
  }),
  view: createView(options)
})
