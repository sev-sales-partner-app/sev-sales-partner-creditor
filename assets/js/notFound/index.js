// import { p } from '../util/debug'
import { HomePage, getUrl } from '../util/router'
import { Title } from '../common'
import { PageLayout } from '../layout'

const createView = () => {
  return function NotFound () {
    return [PageLayout,
      [Title, 'Sorry, we could not find what you were looking 4...04'],
      ['div',
        ['a', { href: getUrl(HomePage) }, 'Return to Home Page'] // ,
        // ['div', { onClick: actions.randomClick }, 'Click Me!']
      ]
    ]
  }
}

export const createNotFound = () => {
  return {
    view: createView()
  }
}

export const NotFound = {
  // actions: ({ update, parentActions, getState }) => {
  //   return {
  //     randomClick: () => {
  //       getState(state => {
  //         console.log('got State', state)
  //         update(model => Object.assign({}, model, { clicked: !state.clicked }))
  //       })
  //     }
  //   }
  // },
  view: createView
}
