import Joi from 'joi-browser'

import { $multiplier, furyTo$ } from '../util/furyNumbers'
// import { grossToNet } from '../util'

const datePattern = new RegExp(/^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])/)

const opts = {
  abortEarly: true,
  language: {} // todo translate err messages to de
}

const senderSchema =
  Joi
    .string()
    .label('sender')
    .alphanum()
    .length(42)
    .required()
const recipientSchema =
  Joi
    .string()
    .label('recipient')
    .alphanum()
    .length(42)
    .required()

const createBaseSchema = () => { // old (settleableTotal, vatRate = 0.19) => {
  return Joi
    .number()
    .label('base')
    .min(0.01)
    // .max(Math.round(furyTo$(grossToNet(settleableTotal, vatRate)) * $multiplier) / $multiplier)
    .required()
}
const createValueSchema = settleableTotal => {
  return Joi.number()
    .label('value')
    .min(0.01)
    .max(Math.round(furyTo$(settleableTotal) * $multiplier) / $multiplier)
    .required()
}
const dateSchema =
  Joi
    .string()
    .label('rxDate')
    .regex(datePattern)
    .required()

const rxNumberSchema =
  Joi
    .string()
    .label('rxNumber')
    .required()
const isPayableSchema =
  Joi
    .boolean() // false for invoice,
    .insensitive(false)
    .required()
const termsConfirmedSchema =
  Joi
    .boolean() // false for invoice,
    .valid(true)
    .insensitive(false)
    .required()
const attachmentSchema =
  Joi
    .object()
    .label('attachment')
    .unknown(true)
    .required()
const vatRateSchema =
  Joi
    .number()
    .label('vatRate')
    .allow([0, 0.07, 0.19])
    .required()
const vatValueSchema =
  Joi.number()
    .label('vatValue')

// export function validateSender (value) {
//   const v = senderSchema.validate(value)
//   return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
// }
//
// export function validateRecipient (value) {
//   const v = recipientSchema.validate(value)
//   return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
// }

export function validateBase (value, settleableTotal) {
  const v = createBaseSchema(settleableTotal).validate(value)
  return { value: Number(v.value), error: (v.error == null ? '' : v.error.details[0].message) }
}

export function validateVatRate (value) {
  const v = vatRateSchema.validate(value)
  return { value: Number(v.value), error: (v.error == null ? '' : v.error.details[0].message) }
}

// export function validateVatValue (value) {
//   const v = vatValueSchema.validate(value)
//   return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
// }

export function validateValue (value, settleableTotal) {
  const v = createValueSchema(settleableTotal).validate(value)
  return { value: Number(v.value), error: (v.error == null ? '' : v.error.details[0].message) }
}

export function validateRxDate (value) {
  const v = dateSchema.validate(value)
  return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
}

export function validateRxNumber (value) {
  const v = rxNumberSchema.validate(value)
  return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
}

export function validateIsPayable (value) {
  const v = isPayableSchema.validate(value)
  return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
}

export function validateConfirmation (value) {
  const v = termsConfirmedSchema.validate(value)
  return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
}

export function validateAttachment (value) {
  const v = attachmentSchema.validate(value)
  return { value: v.value, error: (v.error == null ? '' : v.error.details[0].message) }
}

export function validateSettlementForm (obj, settleableTotal) {
  const baseSchema = createBaseSchema(settleableTotal)
  const valueSchema = createValueSchema(settleableTotal)
  const schema = Joi.object({
    sender: senderSchema,
    recipient: recipientSchema,
    isPayable: isPayableSchema,
    rxNumber: rxNumberSchema,
    rxDate: dateSchema,
    base: baseSchema,
    vatRate: vatRateSchema,
    vatValue: vatValueSchema,
    value: valueSchema,
    attachment: attachmentSchema,
    termsConfirmed: termsConfirmedSchema
  })

  const { value, error } = Joi.validate(obj, schema, opts)
  return Object
    .entries(value)
    .reduce((acc, [k, v]) => {
      const err =
        error == null
          ? ''
          : error
            .details
            .filter(e => e.context.key === k || e.context.label === k)
            .reduce((acc, e, i) => {
              return acc.concat(e.message == null ? '' : (i === 0 ? '' : ' ') + e.message)
            }, '')

      acc[k] = { value: v, error: err }
      return acc
    }, {})
}
