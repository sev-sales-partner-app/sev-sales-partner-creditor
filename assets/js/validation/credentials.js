import Joi from 'joi-browser'
import jwtDecode from 'jwt-decode'

import { now } from '../util'

const jwtPattern = new RegExp(/^[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+\.[A-Za-z0-9-_.+/=]*$/)

const usernameSchema =
  Joi
    .string()
    .label('username')
    .email()
    .required()
const salesPartnerIdSchema =
  Joi
    .string()
    .label('salesPartnerId')
    .alphanum()
    .length(32)
    .required()
const tokenSchema =
  Joi
    .string()
    .label('token')
    .regex(jwtPattern)
    .required()
const payloadSchema =
  Joi
    .object()
    .keys({
      extid: Joi.string().email().required(),
      salesPartnerAddress: Joi.string().alphanum().length(42).required(),
      salesPartnerKey: Joi.string().alphanum(),
      iat: Joi.number(),
      exp: Joi.number().greater(now()),
      aud: Joi.string().valid('SEV Sales Partners'),
      iss: Joi.string().valid('SEV')
    })

export const validateSalesPartnerId = (value) => {
  const res = salesPartnerIdSchema.validate(value)
  return { value: res.value, error: (res.error == null ? '' : res.error.details[0].message) }
}

export function validateUsername (value) {
  const res = usernameSchema.validate(value)
  return { value: res.value, error: (res.error == null ? '' : res.error.details[0].message) }
}

export function validateCredentials (usernameVal, salesPartnerIdVal) {
  return {
    username: validateUsername(usernameVal),
    salesPartnerId: validateSalesPartnerId(salesPartnerIdVal)
  }
}

export const validateToken = (value) => {
  const res = tokenSchema.validate(value)
  return { value: res.value, error: (res.error == null ? '' : res.error.details[0].message) }
}

export const validateTokenPayload = (token) => {
  // console.debug('[validateToken]', 'input', token)
  const payload = (() => {
    try {
      if (typeof token === 'string') return jwtDecode(token)
      if (typeof token === 'object') return token
      return { error: new Error('Token neither string nor object') }
    } catch (error) {
      return { error }
    }
  })()
  // console.debug('[validateToken]', 'payload', payload)
  let opts = { abortEarly: true, allowUnknown: false }
  const res = payloadSchema.validate(payload, opts)
  // console.debug('[validateToken]', 'res', res)
  return { value: res.value, error: (res.error == null ? '' : res.error.details[0].message) }
}
