// import patchConsole from '../util/debug'
import { listenToRouteChanges, parseUrl } from '../util/router'
import { wirem } from '../util/wirem'

import * as furyRpc from '../data/furyRpc'
import { fetchPersistedState, matchBlockTime } from '../data/localStore'

import {
  /* createRoot, */
  Root
} from '../root'

function wireApp (update, getState, data) {
  const Offline = { view: () => function Offline () { return ['div', 'No internet connection!'] } }
  return wirem({
    component: data.onLine === false ? Offline : Root,
    update,
    getState,
    properties: { model: Root.model(data) },
    combinators: {
      state: list => model => list.reduce((x, f) => f(x), model), // sync state handler
      // state: list => model => list.reduce((p, f) => p.then(x => f(x)), Promise.resolve(model)), // async state handler
      // https://medium.com/@Carmichaelize/javascript-testing-for-promises-6c834afc33a8
      // state: list => model => list.reduce((acc, f) => Promise.resolve(acc) === acc ? acc.then(x => f(x)) : f(acc), model), // mix
      nextAction: list => {
        const listWithUpdate = list.map(item => item(update))
        return (state) => listWithUpdate.forEach(item => item(state))
      }
    }
  })
}

// export const createApp = (update, getState) => {
//   // start router and parse initial url
//   listenToRouteChanges(update)
//   const data = parseUrl()
//
//   return Promise.all([
//     fetchPersistedState(),
//     furyRpc
//       .fetchCurrentBlockNumber()
//       .then(matchBlockTime)
//   ])
//     .then(([ persistedState, consensus ]) => {
//       return createRoot({ update, getState, data: Object.assign(persistedState, data, { consensus }) })
//     })
//     .catch(err => {
//       console.log(err)
//     })
// }

export const createAppALT = (update, getState) => {
  // patchConsole()

  // start router and parse initial url
  listenToRouteChanges(update)
  const data = parseUrl()

  return Promise.all([
    fetchPersistedState(),
    furyRpc
      .fetchCurrentBlockNumber()
      .then(matchBlockTime)
  ])
    .then(([ persistedState, consensus ]) => {
      return wireApp(update, getState, Object.assign(persistedState, data, { consensus }))
    })
    .catch(err => {
      console.error(err)
      return wireApp(update, getState, Object.assign(data, { onLine: window.navigator.onLine }))
    })
}

/*

import { createRoot } from "../root"
import { credentialsApi } from "../services"
import { wirem } from "../util/wirem"

const wireApp = (update, getState, user) =>
  wirem({
    component: createRoot,
    data: { user },
    update,
    getState
  })

export const createApp = (update, getState) => Promise.all([
  credentialsApi.getUser()
]).then(([user]) => wireApp(update, getState, user))
  .catch(() => wireApp(update, getState))

 */
