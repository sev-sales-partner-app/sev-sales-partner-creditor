import axios from 'axios'

import { apiHost } from '../util/constants'

// serverApi

const agentApi = axios.create({
  baseURL: apiHost,
  timeout: 9000
})
// axios.defaults.baseURL = 'http://sev.local:3000/api'

/**
 *
 * @param token
 * @return {AxiosPromise<any>}
 */
function checkAuth (token) {
  return agentApi.get('/auth/check', {
    headers: { 'Authorization': token }
  })
}

function perpetuateAuth (token) {
  return agentApi.get('/auth/perpetuation', {
    headers: { 'Authorization': token }
  })
}

/**
 * @param {Object} d - data
 * @param {string} d.username
 * @param {string} d.salesPartnerId
 * @param {string} d.callbackUrl
 * @return {AxiosPromise<any>}
 */
function submitCredentials ({
  username,
  salesPartnerId,
  callbackUrl
}) { return agentApi.post('/auth', { username, salesPartnerId, callbackUrl }) }

function fetchSalesPartnerId (token, salesPartnerAddress) {
  // console.debug(token, salesPartnerAddress)
  return (
    agentApi.get(`/auth/${salesPartnerAddress}`, {
      headers: { 'Authorization': token }
    })
  )
}

/**
 *
 * @param {FormData} formData
 * @param {string} token
 * @return {AxiosPromise<any>}
 */
function submitSettlement (formData, token) {
  return agentApi.put('/settlement', formData, {
    headers: {
      'Authorization': token,
      'Content-Type': 'multipart/form-data'
    }
  })
}

/**
 *
 * @param {Error} err
 */
const logError = (err) => {
  if (err.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.error(err.response.data)
    console.error(err.response.status)
    console.error(err.response.headers)
  } else if (err.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.error(err.request)
  } else {
    // Something happened in setting up the request that triggered an Error
    console.error('Error', err.message)
  }
  console.error(err)
  console.log(err.config)
}

export default {
  checkAuth,
  perpetuateAuth,
  submitCredentials,
  submitSettlement,
  fetchSalesPartnerId,
  logError
}
