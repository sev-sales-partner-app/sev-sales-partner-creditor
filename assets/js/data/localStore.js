import { mergeDeepRight } from '../util/fp'
import * as furyRpc from './furyRpc'

function clearAll () {
  return new Promise((resolve, reject) => {
    try {
      resolve(window.localStorage.clear())
    } catch (e) {
      reject(e)
    }
  })
}

function clearAuth () {
  return new Promise((resolve, reject) => {
    try {
      window.localStorage.removeItem('App::auth')
      window.localStorage.removeItem('App::state')
      resolve('removed App from local store')
    } catch (e) {
      reject(e)
    }
  })
}

function clearState () {
  return new Promise((resolve, reject) => {
    try {
      window.localStorage.removeItem('App::state')
      resolve('removed App::state from local store')
    } catch (e) {
      reject(e)
    }
  })
}

function fetchBlockTime (blockNumber) {
  return new Promise((resolve) => {
    try {
      let res = window.localStorage.getItem('block::' + blockNumber)
      // console.debug('[fetchBlockTime :: localStore]', 'res', res)
      resolve(res != null ? Number.parseInt(res, 10) : res)
    } catch (e) {
      console.error('[fetchBlockTime :: localStore] ', e)
      resolve(null)
    }
  })
}

export function fetchPersistedState () {
  return new Promise((resolve, reject) => {
    try {
      let auth = JSON.parse(window.localStorage.getItem('App::auth') || '{}')
      let model = JSON.parse(window.localStorage.getItem('App::state') || '{}')
      let state = Object.assign({}, model, { auth: mergeDeepRight(model.auth, auth) })
      resolve(state || {})
    } catch (e) {
      reject(e)
    }
  })
}

export function matchBlockTime (blockNum) {
  return fetchBlockTime(blockNum)
    .then(t1 => (t1 != null)
      ? { block: blockNum, time: t1 }
      : furyRpc
        .fetchBlockTimestamp(blockNum)
        .then(t2 => {
          setBlockTime(blockNum, t2)
            .catch(err =>
              console.error('[matchBlockTime :: localStore] error', err))
          return { block: blockNum, time: t2 }
        })
    )
}

function persistAppState (modelProps = {}) {
  return new Promise((resolve, reject) => {
    try {
      if ('auth' in modelProps) delete modelProps.auth
      let read = window.localStorage.getItem('App::state')
      let state = mergeDeepRight(read ? JSON.parse(read) : {}, modelProps)
      window.localStorage.setItem('App::state', JSON.stringify(state))
      resolve(state)
    } catch (e) {
      reject(e)
    }
  })
}

function persistAuth (authObj = {}) {
  return new Promise((resolve, reject) => {
    try {
      let read = JSON.parse(window.localStorage.getItem('App::auth')) || {}
      let auth = mergeDeepRight(read, authObj)
      window.localStorage.setItem('App::auth', JSON.stringify(auth))
      resolve(auth)
    } catch (e) {
      reject(e)
    }
  })
}

function setBlockTime (blockNumber, ts) {
  return new Promise((resolve, reject) => {
    try {
      resolve(window.localStorage.setItem('block::' + blockNumber, ts.toString()))
    } catch (e) {
      console.error('[setBlockTime :: localStore]', e)
      reject(e)
    }
  })
}

export default {
  clearAll,
  clearAuth,
  clearState,
  fetchBlockTime,
  fetchPersistedState,
  matchBlockTime,
  persistAuth,
  persistAppState,
  setBlockTime
}
