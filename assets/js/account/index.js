import { HomePage, SettlementsAccount, QueriedAccount, UserAccount } from '../util/constants'
import { Spinner, Title } from '../common'
import { PageLayout } from '../layout'

import { createAccountSummary, AccountSummary } from './summary'
import { createTxsList, createRxsList, RxsList, TxsList } from './txsList'

const createView = ({ actions, summary, transactions, settlements }) => {
  return function Account (state) {
    const {
      accounts,
      consensus,
      pageId,
      params
    } = state
    // accounts._status !== 'fetching' && actions.maybeFetchBalances(accounts, consensus.block)
    const id = pageId === QueriedAccount ? (params.ledger + '::' + params.address).toString() : pageId
    const ref = accounts[id] ? id : UserAccount
    const account = accounts[ref] || accounts[UserAccount]

    const onFetchBalances = () => actions.maybeFetchBalances(accounts, consensus.block)
    const onFetchTransactions = () => actions.maybeFetchTransactions(ref, account, consensus.block)
    const xs = account.txs || account.rxs

    return ([PageLayout,
      [Title, 'Account' + ' ' + (account.address || params.address || '')],

      pageId !== QueriedAccount && summary(state),

      (onFetchBalances() || onFetchTransactions() || account._status === 'fetching') && (xs == null)
        ? (pageId !== HomePage) && [Spinner, `Loading ${pageId === SettlementsAccount ? 'Settlements' : 'Transactions'}`]
        : pageId === SettlementsAccount
          ? settlements(state)
          : (pageId !== HomePage) && transactions(state)
    ])
  }
}

export const createAccount = ({ parentActions }) => {
  const components = {
    summary: createAccountSummary({ parentActions }).view,
    transactions: createTxsList({ parentActions }).view,
    settlements: createRxsList({ parentActions }).view
  }
  return {
    view: createView(Object.assign({ actions: parentActions }, components))
  }
}

export const Account = {
  dependencies: {
    summary: AccountSummary,
    transactions: TxsList,
    settlements: RxsList
  },
  actions: ({ parentActions }) => parentActions,
  view: createView
}
