import { furyTo$ } from '../util/furyNumbers'
import { SettlementPage, getUrl } from '../util/router'
import { b as css, theme } from '../util/view'

import { computeCommission } from '../computes'
import { Button } from '../common'

const { space, fontSizes } = theme

const createView = () => {
  return function Summary (state) {
    const { accounts } = state
    const {
      entitlement,
      entitlementTotal,
      receivableTotal
    } = computeCommission(accounts)

    const styles = {

      outer: css`
        display: flex;
        flex-wrap: wrap`,

      dlist: css`
        margin: ${space[2]} ${space[5]} ${space[2]} 0`
        .$lastChild(css`margin-right: auto;`),

      key: css`
        font-size: ${fontSizes[1]}
        font-weight: normal;
        line-height: 1.25em`,

      value: css`
        font-family: Courier
        font-size: ${fontSizes[4]};
        font-weight: 600;
        line-height: 1.25em;
        margin-inline-start: 0`,

      suffix: css`
        font-size: ${fontSizes[0]};
        font-weight: normal;
        margin-inline-start: 0
        line-height: 1.25em`,

      mono: css`
        font-family: Courier`
    }

    // actions.maybeFetchBalances(accounts, consensus.block) // happens upstream
    // Stats widget
    return ['section.g-AccountSummary',
      ['div' + '.CommissionData' + styles.outer,
        ['dl' + styles.dlist,

          ['dt' + styles.key,
            'Provisionsanspruch'],

          ['dd' + styles.value,
            ['span' + styles.mono, furyTo$.string(entitlementTotal)]],

          ['dd' + styles.suffix,
            ['span' + styles.mono, furyTo$.string(entitlement)],
            ['span', ' (netto)']
          ]
        ],

        ['dl' + styles.dlist,

          ['dt' + styles.key,
            'davon bereits abgerechnet'],

          ['dd' + styles.value,
            ['span' + styles.mono, furyTo$.string(receivableTotal)]]
        ]
      ],

      [Button,
        { color: 'blue',
          size: 'large',
          css: `margin: ${space[3]} 0`,
          onClick: () => {
            window.location.href =
              getUrl(SettlementPage, { msgId: 'new' })
          }
        },
        ' + Neue Provisionsabrechnung'
      ]
    ]
  }
}

export const createAccountSummary = ({ parentActions }) => {
  return {
    view: createView({ actions: parentActions })
  }
}

export const AccountSummary = {
  actions: ({ parentActions }) => parentActions,
  view: createView
}
