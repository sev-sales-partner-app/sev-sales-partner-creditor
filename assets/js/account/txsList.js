// import { p } from '../util/debug'
import { QueriedAccount, SettlementPage, SettlementsAccount, UserAccount, getUrl } from '../util/router'
import { furyTo$ } from '../util/furyNumbers'
import {
  calcBalance,
  calcVBalance,
  filterTxs,
  fromOrSender,
  isInvoic,
  isPayableFor,
  toOrRecipient
} from '../computes'

import {
  Table,
  Thead,
  TheadRow,
  TheadCell,
  Tbody,
  EmptyRow,
  TbodyRow,
  TbodyCell,
  ShowMoreCell,
  Tfoot,
  TfootRow,
  TfootCell
} from '../common/table'

const createTxsView = ({ actions }) => {
  return function Transactions (state) {
    const { accounts, pageId, params } = state

    let id = pageId === QueriedAccount ? (params.ledger + '::' + params.address).toString() : pageId
    const ref = accounts[id] ? id : UserAccount
    const account = accounts[ref]

    // const {
    //   ref,
    //   label,
    //   address,
    //   ledger,
    //   debit,
    //   credit,
    //   txs,
    //   rxs,
    //   _status,
    //   _lastFetch
    // } = account

    console.assert(
      ref === id || ref === UserAccount,
      '[Transactions :: component]', 'Account ref:', ref, 'should equal', id, 'or', UserAccount
    )

    const accountBalance = calcBalance(account)
    const vBalance = calcVBalance(account)
    const startBalance = accountBalance - vBalance
    const filteredTxs = filterTxs(params.filter, account)

    return (['div', { className: 'nr2 nl2 ma0-ns overflow-y-hidden overflow-x-auto' },
      [Table,

        [Thead,
          [TheadRow,
            [TheadCell,
              { colSpan: '4',
                isnegative: (accountBalance < 0) ? 'true' : '',
                label: 'Saldo ' + (account.label || '') },
              furyTo$.string(accountBalance)
            ]
          ]
        ],

        [Tbody,
          filteredTxs.length === 0
            ? [EmptyRow,
              { colSpan: '4' },
              account._status === 'loading' ? 'fetching...' : 'no txs to display']

            : filteredTxs.map((tx, i) => {
              const isPayableTx = isPayableFor(account.address)(tx)
              const toOrRecipientTx = toOrRecipient(tx)
              const fromOrSenderTx = fromOrSender(tx)

              // ['tr.db.dt-row-l.hover-bg-near-white' + (i !== 0 ? '.bt.b--light-gray' : '.bn')
              return [TbodyRow,
                { key: i },

                [TbodyCell,
                  { label: 'Buchungsdatum' },
                  new Date(tx.blockTime * 1000).toLocaleDateString('de-DE')],

                [TbodyCell,
                  { label: 'Type' },
                  isPayableTx ? 'debit (payable)' : 'credit (receivable)'],

                isPayableTx
                  ? [TbodyCell,
                    { label: 'To' },
                    ['a',
                      { href:
                          getUrl(QueriedAccount,
                            { address: tx[toOrRecipientTx], ledger: account.ledger }) },
                      tx[toOrRecipientTx]
                    ]]
                  : [TbodyCell,
                    { label: 'From' },
                    ['a',
                      { href:
                          getUrl(QueriedAccount,
                            { address: tx[fromOrSenderTx], ledger: account.ledger })
                      },
                      tx[fromOrSenderTx]]],

                [TbodyCell,
                  isPayableTx ? { isnegative: 'true', align: 'right' } : { align: 'right' },
                  (isPayableTx ? '-' : '') +
                  furyTo$.string(Number.parseInt(tx.value, 16))]
              ]
            }).concat([
              [TbodyRow,
                [ShowMoreCell,
                  { disabled: account._status === 'fetching_more' ? 'disabled' : '',
                    fromblock: String(account._lastFetch.blocks[0]),
                    txscount: String(filteredTxs.length),
                    onClick: () =>
                      actions.fetchMoreTxs(ref, account, {
                        threshold: account.txs.length * 2,
                        ledgerType: ref === SettlementsAccount ? 'transferable' : 'stromkonto'
                      }) },
                  account._status === 'fetching_more' ? 'Loading...' : 'Load More Transactions'
                ]
              ]
            ])
        ],

        [Tfoot,
          [TfootRow,
            [TfootCell,
              { colSpan: '4',
                isnegative: (accountBalance < 0) ? 'true' : '',
                label: 'Anfangssaldo' },
              furyTo$.string(startBalance)]
          ]
        ]
      ]
    ])
  }
}

const createRxsView = ({ actions }) => {
  return function Settlements (state) {
    const { accounts, params } = state
    // let id = pageId === QueriedAccount ? (params.ledger + '::' + params.address).toString() : pageId
    // const ref = accounts[id] ? id : SettlementsAccount
    const ref = SettlementsAccount
    const account = accounts[SettlementsAccount]

    const accountBalance = calcBalance(account) * -1 // Note: reversing transferable balance
    const vBalance = calcVBalance(account)
    const startBalance = accountBalance - vBalance
    const filteredTxs = filterTxs(params.filter, account)

    console.log(filteredTxs.length, account.rxs.length)
    return (['div', { className: 'nr2 nl2 ma0-ns overflow-y-hidden overflow-x-auto' },
      [Table,
        [Thead,
          [TheadRow,
            [TheadCell,
              { colSpan: '4',
                isnegative: (accountBalance < 0) ? 'true' : '',
                label: 'Saldo ' + (account.label || '') },
              furyTo$.string(accountBalance)
            ]
          ]
        ],

        [Tbody,
          (filteredTxs.length === 0)
            ? [EmptyRow,
              { colSpan: '4' },
              account._status === 'loading' ? 'fetching...' : 'no receipts to display']

            : filteredTxs.map((tx, i) => {
              const isPayableTx = isPayableFor(account.address)(tx)
              const toOrRecipientTx = toOrRecipient(tx)
              const fromOrSenderTx = fromOrSender(tx)

              return [TbodyRow,
                { key: i,
                  onClick: () => window.location.assign(getUrl(SettlementPage, { msgId: tx.msg })) },

                [TbodyCell,
                  { label: 'Buchungsdatum' },
                  new Date(tx.blockTime * 1000).toLocaleString('de-DE')],

                [TbodyCell,
                  { label: 'Type' },
                  isInvoic(tx.data)
                    ? `invoic (${isPayableTx ? 'payable' : 'receivable'})`
                    : `remadv (${isPayableTx ? 'payable' : 'receivable'})`],

                ((isPayableTx && tx[fromOrSenderTx].toLowerCase() !== account.address.toLowerCase()) ||
                (!isPayableTx && tx[toOrRecipientTx].toLowerCase() === account.address.toLowerCase()))
                  ? [TbodyCell, { label: 'From' }, tx[fromOrSenderTx]]
                  : [TbodyCell, { label: 'To' }, tx[toOrRecipientTx]],

                [TbodyCell,
                  { isnegative: isPayableTx ? 'true' : '', align: 'right' },
                  (isPayableTx ? '-' : '') +
                  furyTo$.string(Number.parseInt(tx.value, 16))]
              ]
            }).concat([
              [TbodyRow,
                { key: account.rxs.length },
                [ShowMoreCell,
                  { disabled: (account._status === 'fetching_more') ? 'disabled' : '',
                    fromblock: String(account._lastFetch.blocks[0]),
                    txscount: String(filteredTxs.length),
                    onClick: () =>
                      actions.fetchMoreTxs(ref, account, {
                        threshold: account.rxs.length * 2,
                        ledgerType: ref === SettlementsAccount ? 'transferable' : 'stromkonto'
                      }) },
                  account._status === 'fetching_more' ? 'Loading...' : 'Load More Settlements'
                ]
              ]
            ])
        ],

        [Tfoot,
          [TfootRow,
            [TfootCell,
              { colSpan: '4',
                isnegative: (accountBalance < 0) ? 'true' : '',
                label: 'Anfangssaldo' },
              furyTo$.string(startBalance)
            ]
          ]
        ]
      ]
    ])
  }
}

export const createTxsList = ({ parentActions }) => {
  return {
    view: createTxsView({ actions: parentActions })
  }
}

export const createRxsList = ({ parentActions }) => {
  return {
    view: createRxsView({ actions: parentActions })
  }
}

export const TxsList = {
  actions: ({ parentActions }) => parentActions,
  view: createTxsView
}

export const RxsList = {
  actions: ({ parentActions }) => parentActions,
  view: createRxsView
}
