import { furyTo$ } from '../../util/furyNumbers'
import { computeCommission } from '../../computes/index'
import { Spinner } from '../../common'
import {
  Table,
  Thead,
  TheadRow,
  TheadCell,
  Tbody,
  TbodyRow,
  TbodyCell,
  Tfoot,
  TfootRow,
  TfootCell
} from '../../common/table'

const createView = () => {
  return function Settleable (state) {
    const {
      entitlement,
      entitlementTotal,
      debitorBalance,
      settleableTotal
    } = computeCommission(state.accounts)

    // TODO [Settleable] turn into components consolidate w/ [account/table]
    return state.accounts._status === 'fetching' && entitlementTotal === 0
      ? [Spinner, 'loading settlement data...']
      : [Table,
        [Thead,
          [TheadRow,
            [TheadCell,
              { colSpan: '4',
                label: 'Provision' }]]],

        [Tbody,
          [TbodyRow,

            [TbodyCell,
              { label: 'Zeitpunkt' },
              new Date().toLocaleString('de-DE')],

            [TbodyCell,
              { label: 'offener Provisionsanspruch' },
              furyTo$.string(entitlement)],

            [TbodyCell,
              { label: 'USt. 19 %' },
              furyTo$.string(entitlementTotal - entitlement)],

            [TbodyCell,
              { align: 'right',
                isnegative: entitlementTotal < 0 ? 'true' : '' },
              furyTo$.string(entitlementTotal)]],

          furyTo$(debitorBalance) !== 0 &&
            [TbodyRow,
              [TbodyCell,
                { colSpan: '3',
                  label: debitorBalance < 0
                    ? 'davon bereits abgerechnet'
                    : 'Noch nicht abgerechneter Anspruch aus Vorperiode' }],
              [TbodyCell,
                { align: 'right',
                  colSpan: '1',
                  isnegative: debitorBalance < 0 ? 'true' : '' },
                furyTo$.string(debitorBalance)]]
        ],

        [Tfoot,
          [TfootRow,
            [TfootCell,
              { label: 'Abrechnungsfähiger Betrag (brutto)',
                isnegative: settleableTotal < 0 ? 'true' : '',
                colSpan: '4' },
              furyTo$.string(settleableTotal)]]
        ]
      ]
  }
}

export const createSettleable = () => {
  return {
    view: createView()
  }
}

export const Settleable = {
  view: createView
}
