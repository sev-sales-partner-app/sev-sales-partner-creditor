import { SettlementPage, SettlementsAccount } from '../util/router'

import * as furyRpc from '../data/furyRpc'
import localStore from '../data/localStore'
import serverApi from '../data/serverApi'

import { Title } from '../common'
import { PageLayout } from '../layout'

import { createSettlementForm, SettlementForm } from './form/index'
import { createSettlementDetail, SettlementDetail } from './detail/index'

import { createActions } from './actions'

const createModel = (formComponent) => {
  return {
    [SettlementPage]: Object.assign({
      status: '',
      label: 'Provisionsabrechnung'
    }, formComponent.model())
  }
}

const createView = ({ actions, form, detail }) => {
  return function Settlement (state) {
    const {
      accounts,
      consensus,
      params
    } = state
    const ref = SettlementsAccount
    const account = accounts[ref]
    const rxs = account.rxs || []
    const Rx = rxs.find(rx => rx.msg === params.msgId) || {}
    const onFetch = () => actions.maybeFetchBalances(accounts, consensus.block)
    const onFetchMore = (Rx) =>
      params.msgId !== 'new'
        ? actions.maybeFetchMoreTransactions(ref, account, Rx, consensus.block) : false

    onFetch() || onFetchMore(Rx)

    return ([PageLayout,
      [Title,
        params.msgId === 'new'
          ? 'New Settlement'
          : 'Settlement ' + (params.msgId || '')
      ],
      params.msgId === 'new'
        ? form(state)
        : detail(state)
    ])
  }
}

export const createSettlement = ({ parentActions, getState, update }) => {
  const actions = createActions({ furyRpc, localStore, serverApi })({ update, parentActions })
  const Form = createSettlementForm({ id: SettlementPage })({ parentActions: actions, getState, update })
  const components = {
    form: Form.view,
    detail: createSettlementDetail({ parentActions: actions }).view
  }

  return {
    model: () => createModel(Form.model()),
    view: createView(Object.assign({ actions }, components))
  }
}

const dependencies = {
  form: SettlementForm({ id: SettlementPage }),
  detail: SettlementDetail
}

export const Settlement = {
  dependencies,
  actions: createActions({ furyRpc, localStore, serverApi }),
  model: () => createModel(dependencies.form),
  view: createView
}
