/* global FormData */
import { p } from '../util/debug'
import { assoc, assocPath, compose, prop } from '../util/fp'
import { $toFury } from '../util/furyNumbers'
import { netToGross } from '../util'
import { DebitorAccount, SettlementsAccount, SettlementPage, UserAccount, getUrl } from '../util/router'
import { isInvoic, isPayableFor } from '../computes'

// helper fns

function dataToTx (data, accountsByRef) {
  const {
    sender,
    recipient,
    base,
    vatRate,
    // value,
    rxDate,
    rxNumber,
    isPayable,
    termsConfirmed,
    attachment
  } = data

  const _$sender =
    Object.entries(accountsByRef)
      .filter(([k, v]) => v.address === sender)
      .find(([k]) => k === UserAccount)[1]

  const _$recipient =
    Object
      .entries(accountsByRef)
      .filter(([k, v]) => v.address === recipient)
      .find(([k]) => k === DebitorAccount)[1]

  // p('[dataToTx :: action]',_$sender', _$sender)
  // p('[dataToTx :: action]','_$recipient', _$recipient)
  if ('txs' in _$sender) delete _$sender.txs
  if ('txs' in _$recipient) delete _$recipient.txs

  try {
    return {
      _$recipient,
      _$sender,
      isPayable,
      ledger: _$sender.ledger,
      from: isPayable ? _$sender.address : _$recipient.address, // note invoic Tx address switch
      to: isPayable ? _$recipient.address : _$sender.address,
      base: $toFury(base),
      value: compose(Math.round, netToGross)($toFury(base), vatRate), // Math.round(netToGross($toFury(base), vatRate)),
      text: JSON.stringify({
        rxNumber,
        rxDate,
        Datei: attachment.name,
        Bestaetigung: termsConfirmed,
        Ust: vatRate
      })
    }
  } catch (e) {
    console.error(e)
    throw new Error('[dataToTx :: action] Tx assembly failed')
  }
}

// function generateDataUrl (fileObj) {
//   return new Promise((resolve, reject) => {
//     const reader = new FileReader()
//     reader.onload = ev => resolve(ev.target.result)
//     reader.onerror = err => reject(err)
//
//     reader.readAsDataURL(fileObj)
//   })
// }

export const createActions = ({ furyRpc = {}, localStore = {}, serverApi = {} }) => ({ update, parentActions }) => {
  function setStatus (message) {
    update(assocPath([ SettlementPage, 'status' ])(message))
  }

  // processSettlement :: (Object, Object, Object, String) -> void 0
  function processSettlement (validatedData = {}, commission = {}, accountsByRef = {}, token) {
    const _debugId = '[processSettlement :: action]'
    const user = accountsByRef[UserAccount]
    const nodeFn = () => furyRpc.createNode(user)
    const formData = new FormData()
    const data = Object.entries(validatedData)
      .reduce((acc, [k, v]) => {
        acc[k] = v.value
        formData.append(k, v.value)
        return acc
      }, {})

    // p(_debugId, 'validated data', validatedData)
    // for (let [k, v] of formData.entries()) {
    //   p(_debugId, 'FormData entry', `${k}: ${v}`, typeof v)
    // }
    const tx = dataToTx(data, accountsByRef)

    try {
      let assert = console.assert
      assert(
        nodeFn().wallet.address.toLowerCase() === user.address && user.address === tx._$sender.address,
        _debugId + ' ' + ' Assertion (1) node.wallet.address should equal user.address'
      )
      assert(
        tx.ledger === user.ledger && tx.ledger === accountsByRef[DebitorAccount].ledger,
        _debugId + ' ' + ' Assertion 2. tx.ledger === user.ledger'
      )
      assert(
        tx.to === user.address,
        _debugId + ' ' + ' Assertion (3). tx.to === user.address'
      )
      assert(
        tx.from === accountsByRef[DebitorAccount].address,
        _debugId + ' ' + ' Assertion (4) tx.to === accountRef[DebitorAccount].address'
      )
      assert(
        tx.base <= commission.entitlement,
        _debugId + ' ' + ' Assertion (5) tx.base <= commission.entitlement'
      )
      assert(
        tx.value <= commission.settleableTotal,
        _debugId + ' ' + ' Assertion (6) tx.value <= commission.settleableTotal ' + tx.value + ' <= ' + commission.settleableTotal
      )
      assert(
        tx.value === Math.round(tx.base + tx.base * JSON.parse(tx.text).Ust),
        _debugId + ' ' + ' Assertion (7) tx.value === tx.base + tx.base * tx.text.Ust  ' +
        tx.value + ' === Math.round(' + tx.base + ' + ' + tx.base + ' * ' + JSON.parse(tx.text).Ust + ')'
      )
    } catch (e) {
      console.error(_debugId + ' Assertion failed', e)
      return
    }

    const node = nodeFn()
    const msg =
      compose(window.btoa, JSON.stringify,
        tx => Object
          .entries(tx)
          .filter(([k]) => k !== '_$recipient')
          .filter(([k]) => k !== '_$sender')
          .filter(([k]) => k !== 'isPayable')
          .filter(([k]) => k !== 'ledger')
          .reduce((acc, [k, v]) => { acc[k] = v; return acc }, {}))(tx)

    p(_debugId, 'submitting tx: ', tx)
    p(_debugId, 'with msg: ', msg)

    let txHashes = []

    setStatus('Submitting transaction to ledger')

    // console.log('TX to send', tx)
    serverApi
      .checkAuth(token)
      .then(() => furyRpc.addTxToLedger(node, tx), err => {
        update(compose(
          assoc('auth')({
            authenticated: false,
            username: { value: '', error: '' },
            salesPartnerId: { value: '', error: '' },
            message: 'Bitte neu einloggen, um fortzufahren.',
            status: '',
            token: ''
          }),
          assocPath([ SettlementPage, 'status' ])('')
        ))
        localStore.clearAuth()
          .catch(err => console.error(err))
        throw err
      })
      .then(txh => {
        txHashes.push(txh)
        p(_debugId, 'creating transaction message')
        setStatus('creating transaction message')
        return furyRpc.createStringStore(node, msg)
      })
      .then(msgAddress => {
        p(_debugId, 'sending message to recipient: ' + (tx.isPayable ? tx.to : tx.from) +
          ' (' + accountsByRef[DebitorAccount].label + ')')
        setStatus('sending message to recipient: ' + (tx.isPayable ? tx.to : tx.from) +
          ' (' + accountsByRef[DebitorAccount].label + ')')

        formData.append('msgAddress', msgAddress.toLowerCase())
        return furyRpc
          .sendRxToRecipient(node, {
            ledger: accountsByRef[SettlementsAccount].ledger,
            recipient: tx.isPayable ? tx.to : tx.from,
            msgAddress,
            base: tx.base,
            value: tx.value,
            isPayable: tx.isPayable
          })
      })
      .then(rxh => {
        const formReset = (id) => function (x) {
          return compose(...Object
            .keys(prop(id)(x))
            .filter(k => (k !== 'status'))
            .filter(k => (k !== 'label'))
            .filter(k => k !== 'vatRate')
            .flatMap(k => assocPath([ id, k ])({ value: '', error: '' })))(x)
        }

        p(_debugId, 'update settlement status & navigate to Settlement Account page')
        update(compose(
          assocPath([ SettlementPage, 'status' ])(''),
          assoc('url')(getUrl(SettlementsAccount, { filter: 'sent' })),
          assocPath([ 'params', 'filter' ])('sent'),
          assoc('pageId')(SettlementsAccount),
          formReset(SettlementPage)
        ))

        txHashes.push(rxh)
        formData.append('tx', JSON.stringify(tx))
        formData.append('txHashes', JSON.stringify(txHashes))

        // p(_debugId, 'ready to send e-mail via req to server:')
        // for (let [k, v] of formData.entries()) {
        //   p(_debugId, 'FormData entry', `{ ${k}: ${v} }`, typeof v)
        // }

        serverApi
          .submitSettlement(formData, token)
          .then(res => p(_debugId, 'SEV Agent response: ', res))
          .catch(err => {
            serverApi.logError(err)
            throw err
          })
      })
      .catch(err => {
        console.error(err)
        let status
        if (err.response) {
          status = err.response.data.message
        } else if (err.request) {
          status = 'Server did not respond.'
        } else {
          status = err.message
        }
        setStatus(status)
      })
  }

  let countdown
  function count () {
    let c = 60
    countdown = window.setInterval(() => {
      c = c - 1
      console.log(c, 'secs')
      // update(model => Object.assign({}, model, { SECS: c }))
      update(assocPath([ SettlementPage, 'status' ])(
        'Crediting invoic! ... Be patient, this may take a minute. (' + c + ' secs)'))
    }, 1000)
  }

  // creditInvoic :: (Object, Object?, Object, Object?) -> void 0
  function creditInvoic (userAccount = {}, settlementsAccount = {}, rx, messageText = {}) {
    const { extid, ledger, address } = userAccount
    const rxMessage = rx.message;

    [[
      typeof extid === 'string',
      settlementsAccount.ledger != null,
      isInvoic(rx.data),
      !isPayableFor(address)(rx)
    ]]
      .map(conditions => conditions.every(Boolean))
      .map(() => {
        p('[creditInvoic :: action] set state.' + SettlementPage + '.status to "Crediting invoice..."')
        // update(assocPath([ SettlementPage, 'status' ])('Crediting invoic! ... Be patient, this may take a minute.'))
        update(assocPath([ SettlementPage, 'status' ])(
          'Crediting invoic! ... Be patient, this may take a minute. (' + 60 + ' secs)'))
        count()
        furyRpc.addTxWithRx(furyRpc.createNode({ extid }), {
          ledger,
          address,
          rxLedger: settlementsAccount.ledger,
          from: rxMessage.to,
          to: rxMessage.from,
          base: Number(rxMessage.base) || Number.parseInt(rx.base, 16),
          value: Number(rxMessage.value) || Number.parseInt(rx.value, 16),
          text: 'canceling / crediting invoic ' + (messageText.rxNumber || rx.msg),
          isPayable: true
        })
          .then(() => {
            clearInterval(countdown)
            update(compose(
              assocPath([ SettlementPage, 'status' ])(''),
              assoc('url')(getUrl(SettlementsAccount, { filter: 'sent' })),
              assocPath([ 'params', 'filter' ])('sent'),
              assoc('pageId')(SettlementsAccount)
            ))
          })
          .catch(err => {
            console.error('[creditInvoic]', err)
            update(assocPath([ SettlementPage, 'status' ])(''))
          })
      })
  }

  return Object.assign({}, parentActions, {
    processSettlement,
    creditInvoic
  })
}
