/* eslint-disable indent */
// import { p } from '../util/debug'

import { furyTo$ } from '../../util/furyNumbers'
import { SettlementPage, SettlementsAccount, UserAccount } from '../../util/constants'
import { b as css, theme } from '../../util/view'

import { computeCommission, isInvoic, isPayableFor } from '../../computes/index'
import { Spinner, Button } from '../../common'

const { colors, fontSizes, space } = theme
const createActions = ({ parentActions }) => parentActions

const createView = ({ actions }) => {
  return function Settlement (state) {
    const {
      accounts,
      params
    } = state
    const settlement = state[SettlementPage]
    // const ref = SettlementsAccount
    const account = accounts[SettlementsAccount]
    const commission = computeCommission(accounts)

    // const {
    //   label,
    //   address,
    //   ledger,
    //   debit,
    //   credit,
    //   txs,
    //   rxs,
    //   _status,
    //   _lastFetch
    // } = account

    const rxs = account.rxs || []
    const rx = rxs.find(rx => rx.msg === params.msgId) || {}
    const rxMessage = rx.message
    const isPayableRx = isPayableFor(account.address)(rx)
    const isInvoicRx = isInvoic(rx.data)
    const rxMessageText = ((rxMessage) => {
      try {
        return rxMessage && rxMessage.text ? JSON.parse(rxMessage.text) : undefined
      } catch (e) {
        console.info('[SettlementDetail :: component] Settlement Message text (rx.message.text) is not an object.')
        return undefined
      }
    })(rxMessage)

    // p('================= DEBUGGING [SettlementDetail] =================')
    // p('+++STATE ARGUMENTS+++')
    // p('account', Object.assign({}, account))
    // p('ctx', Object.assign({}, ctx))
    // p('params', Object.assign({}, params))
    // p('settlement', Object.assign({}, settlement))
    // p('+++PROCESSED STATE +++')
    // p('rxs', [ ...rxs ])
    // p('rx', Object.assign({}, rx))
    // p(Object.assign({}, { rxMessage }))
    // p(Object.assign({}, { rxMessageText }))
    // p(Object.assign({}, { isPayableRx }))
    // p(Object.assign({}, { isInvoicRx }))
    // p('[settlementDetail :: settlement]', 'isFetching', isFetching)
    // p('================= END DEBUGGING [SettlementDetail] =================')

    // mt4 mw7 overflow-x-hidden
    const section = 'section' + css`
      margin-top: ${space[4]};
      max-width: 48rem;
      overflow-x: hidden`

    // collapse courier db dt-l f6 f5-ns tableLayout: 'auto'
    const table = 'table' + css`
      display: block;
      font-family: Courier Next,courier,monospace;
      font-size: ${fontSizes[1]}`
    .$notSmall`
      font-size: ${fontSizes[2]}`
    .$large`
      border-collapse: collapse;
      border-spacing: 0;
      display: table;
      table-layout: auto`

    // ['tbody', { className: 'db dt-row-group-l' },
    const details = 'tbody' + css`
      display: block;`
      .$large`
        display: table-row-group;`

    // db dt-row-group-l bt
    const vat = 'tbody' + css`
      display: block;
      border-top: 1px solid ${colors.midGray}
      margin: ${space[2]} 0 ${space[2]} 0`
      .$large`
        display: table-row-group;`

    const total = 'tbody' + css`
      display: block;
      border-top: 1px solid ${colors.midGray}`
      .$large`
        display: table-row-group;`

    // trow tr.db.dt-row-l
    const row = 'tr' + css`
      display: block;`
      .$large`
        display: table-row;`

    // td.db.dtc-l.f6-m.ph2.pt1.pv1-l
    // td.db.dtc-l.f6-m.ph2.pt1.pb2.pv1-l // message
    // td.db.dtc-l.f6-m.ph2.pt1.pb0.pb2-l
    const key = 'td' + css`
      display: block;
      padding: ${space[1]} ${space[2]} 0 ${space[2]}`
      .$medium`
        font-size: ${fontSizes[1]}`
      .$large`
        display: table-cell;
        padding: ${space[1]} ${space[2]}`

    const key2 = 'td' + css`
      display: inline-block;
      padding: ${space[2]}
      width: 50%`
      .$large`
        display: table-cell;
        padding: ${space[1]} ${space[2]}
        width: auto`

    // td.db.dtc-l.ph2.pb1.pv1-l'
    // td.db.dtc-l.ph2.pb2.pv1-l
    // td.db.dtc-l.ph2.pt1.pb2
    const value = 'td' + css`
      display: block;
      padding: 0 ${space[2]} ${space[1]} ${space[2]}`
      .$large`
        display: table-cell;
        padding: ${space[1]} ${space[2]}`

    const value2 = 'td' + css`
      display: inline-block;
      text-align: right;
      padding: ${space[2]}
      width: 50%`
      .$large`
        display: table-cell;
        padding: ${space[1]} ${space[2]}
        width: auto`

    // TODO disambiguation: rename state.stettlement.status tp state.settlement.statusMsg
    return (account._status === 'fetching_more' || account._status === 'fetching')
      ? [Spinner, 'Searching Settlement' + '...']
      : (rx.msg == null)
        ? ['p', 'Settlement not found']
        : ['div',
          (isPayableRx && isInvoicRx) &&
            // flex.flex-row.items-center.justify-end
            ['div' + css`
              align-items: center;
              display: flex;
              flex-direction: row;
              justify-content: flex-end`,

              settlement.status &&
                // b.f6.mr3
                ['div' + css`
                  font-size: ${fontSizes[1]};
                  font-weight: 600;
                  margin-right: ${space[3]}`,
                  settlement.status],

              // b b--dark-red ba br2 bg-transparent dark-red dim f6 input-reset ph3 pv2
              [Button,
                { disabled: settlement.status !== '',
                  color: 'danger' },
                  // todo onclick () => action
                'Mark as paid']
            ],

          (!isPayableRx && (Number.parseInt(rx.value, 16) <= commission.receivableTotal)) &&
            // flex.flex-row.items-center.justify-end
            ['div' + css`
              align-items: center;
              display: flex;
              flex-direction: row;
              justify-content: flex-end`,

              settlement.status &&
                // b.f6.mr3
                ['div' + css`
                  font-size: ${fontSizes[1]};
                  font-weight: 600;
                  margin-right: ${space[3]}`, settlement.status],

              [Button,
                { disabled: settlement.status !== '',
                  color: 'danger',
                  onClick: () =>
                    actions
                      .creditInvoic(accounts[UserAccount], accounts[SettlementsAccount], rx, rxMessageText) },
                'cancel / credit']
            ],

          [section,
            [table,
              [details,
                (isPayableRx && isInvoicRx) || (!isPayableRx && !isInvoicRx)
                  ? [row,
                    [key,
                      'Sender'],
                    [value,
                      rx.sender]
                  ]
                  : [row,
                    [key,
                      'Recipient'],
                    [value,
                      rx.recipient]
                  ],

                [row,
                  [key,
                    'Type'],
                  [value,
                    (isInvoicRx ? 'INVOIC' : 'REMADV') + (isPayableRx ? ' (payable)' : ' (receivable)')]
                ],

                [row,
                  [key,
                    'Block'],
                  [value,
                    rx.blockNumber + ' @ ' + new Date(rx.blockTime * 1000).toLocaleString('de-DE')]
                ],

                (rxMessageText)
                  ? [
                    [row,
                      [key,
                        'rxDate'],
                      [value,
                        rxMessageText.rxDate || 'n/a']
                    ],
                    [row,
                      [key,
                        'rxNumber'],
                      [value,
                        rxMessageText.rxNumber || 'n/a']
                    ]
                  ]
                  : [row,
                    [key,
                      'text'],
                    [value,
                      rx.message.text || 'n/a']
                  ]
              ],

              [vat,
                [row,
                  [key2, // 'td.dib.dtc-l.ph2.pb1.pt2.w-50.w-auto-l'
                    'base'],
                  [value2, // 'td.dib.dtc-l.ph2.pv1.tr.w-50.w-auto-l',
                    furyTo$.string(Number.parseInt(rx.base, 16))]
                ],

                [row,
                  [key2, // 'td.dib.dtc-l.ph2.pb2.pt1.w-50.w-auto-l',
                    'USt. ' + (rxMessageText && rxMessageText.Ust ? (rxMessageText.Ust * 100) + ' %' : '')],
                  [value2, // 'td.dib.dtc-l.ph2.pb2.pt1.tr.w-50.w-auto-l',
                    furyTo$.string(Number.parseInt(rx.value, 16) - Number.parseInt(rx.base, 16))]
                ]
              ],

              [total,
                [row,
                  [key2,
                    'value'],
                  [value2,
                    furyTo$.string(Number.parseInt(rx.value, 16))]
                ]
              ]
            ]
          ]
        ]
  }
}

export const createSettlementDetail = ({ parentActions }) => {
  return {
    view: createView({ actions: parentActions })
  }
}

export const SettlementDetail = {
  actions: createActions,
  view: createView
}
