/* eslint-disable indent,no-unexpected-multiline,func-call-spacing */
import { p } from '../../util/debug'
import { defaultTo, assocPath, path, prop, mergeDeepLeft, compose, pipe } from '../../util/fp'
import { preventDefault, grossToNet } from '../../util/index'
import { furyTo$, $toFury, $multiplier } from '../../util/furyNumbers'
import {
  SettlementsAccount,
  UserAccount,
  DebitorAccount,
  getUrl
} from '../../util/router'
import { b as css, theme } from '../../util/view'

import {
  InputGroup,
  Input,
  Label,
  Hint,
  Prefix,
  Suffix,
  Option,
  Select,
  Button
} from '../../common'

import {
  validateBase,
  validateVatRate,
  validateValue,
  validateRxDate,
  validateRxNumber,
  validateIsPayable,
  validateConfirmation,
  validateAttachment,
  validateSettlementForm
} from '../../validation/settlement'

import { createSettleable, Settleable } from '../settleable'
import { computeCommission } from '../../computes/index'

const createActions = ({ update, getState, parentActions }) => {
  return {

    toggleIsPayable: function (id, err) {
      return function (ev) {
        const value = ev.target.value
        const isPayable = err ? validateIsPayable(value) : { value, error: err }
        update(compose(
          assocPath([ id, 'isPayable', 'error' ])(isPayable.error),
          assocPath([ id, 'isPayable', 'value' ])(isPayable.value)
        ))
      }
    },

    setRxNumber: function (id, err) {
      return function (ev) {
        const value = ev.target.value
        const rxNum = err ? validateRxNumber(value) : { value, error: err }
        update(compose(
          assocPath([ id, 'rxNumber', 'error' ])(rxNum.error),
          assocPath([ id, 'rxNumber', 'value' ])(rxNum.value)
        ))
      }
    },

    setRxDate: function (id, err) {
      return function (ev) {
        const value = ev.target.value
        const date = err ? validateRxDate(value) : { value, err }
        update(compose(
          assocPath([ id, 'rxDate', 'error' ])(date.err),
          assocPath([ id, 'rxDate', 'value' ])(date.value)
        ))
      }
    },

    setBase: function (id, err, settleableTotal) {
      return function (ev) {
        const value = ev.target.value
        const base = err ? validateBase(value, settleableTotal) : { value, error: err }
        const getRate = model => defaultTo(0.19)(path([ id, 'vatRate', 'value' ])(model))
        const setVat = model =>
          assocPath
            ([ id, 'vatValue', 'value' ])
            (Math.round(Number(base.value) * Number(getRate(model)) * 100) / 100)
            (model)
        const setSum = model =>
          assocPath
            ([ id, 'value', 'value' ])
            (Math.round((Number(base.value) + (Number(base.value) * Number(getRate(model)))) * 100) / 100)
            (model)

        update(compose(
          setSum,
          setVat,
          assocPath([ id, 'base', 'error' ])(base.error),
          assocPath([ id, 'base', 'value' ])(Number(base.value))
        ))
      }
    },

    setVatRate: function (id, err, settleableTotal) {
      return function (ev) {
        const value = ev.target.value
        const rate = err ? validateVatRate(value, settleableTotal) : { value, error: err }
        const getBase = model => defaultTo(0)(path([ id, 'base', 'value' ])(model))
        const setVat = model =>
          assocPath
            ([ id, 'vatValue', 'value' ])
            (Math.round(Number(getBase(model)) * Number(rate.value) * 100) / 100)
            (model)
        const setSum = model =>
          assocPath
            ([ id, 'value', 'value' ])
            (Math.round((Number(getBase(model)) + (Number(rate.value) * Number(getBase(model)))) * 100) / 100)
            (model)
        update(compose(
          setSum,
          setVat,
          assocPath([ id, 'vatRate', 'error' ])(rate.error),
          assocPath([ id, 'vatRate', 'value' ])(Number(rate.value))
        ))
      }
    },

    setValue: function (id, err, settleableTotal) {
      return function (ev) {
        const value = ev.target.value
        const sum = err ? validateValue(value, settleableTotal) : { value, error: err }
        const getRate = model => defaultTo(0.19)(path([ id, 'vatRate', 'value' ])(model))
        const getBase = model => defaultTo(0)(path([ id, 'base', 'value' ])(model))
        const setBase = model =>
          assocPath
            ([ id, 'base', 'value' ])
            (pipe($toFury, x => x / $multiplier)(grossToNet(Number(sum.value), Number(getRate(model)))))
            (model)
        const setVat = model =>
          assocPath
            ([ id, 'vatValue', 'value' ])
            (Math.round((Number(sum.value) - Number(getBase(model))) * 100) / 100)
            (model)
        update(compose(
          setVat,
          setBase,
          assocPath([ id, 'value', 'error' ])(sum.error),
          assocPath([ id, 'value', 'value' ])(Number(sum.value))
        ))
      }
    },

    setAttachment: function (id, err) {
      return function (ev) {
        const value = ev.target.files[0]
        const atmnt = err ? validateAttachment(value) : { value, error: err }
        // if (atmnt.value.size > (1048576)) atmnt.error = 'File must be smaller than 1 MB.'
        update(compose(
          assocPath([ id, 'attachment', 'error' ])(atmnt.error),
          assocPath([ id, 'attachment', 'value' ])(atmnt.value)
        ))
      }
    },

    setConfirmation: function (id, err) {
      return function (ev) {
        const value = ev.target.checked // Note target checked!
        const confirm = err ? validateConfirmation(value) : { value, error: err }
        update(compose(
          assocPath([ id, 'termsConfirmed', 'error' ])(confirm.error),
          assocPath([ id, 'termsConfirmed', 'value' ])(confirm.value)
        ))
      }
    },

    navigateBack: (id, page = SettlementsAccount, params = { filter: 'sent' }) => function () {
      const formReset = (id) => function (model) {
        return compose(
          ...Object
            .keys(prop(id)(model))
            .filter(k => (k !== 'status'))
            .filter(k => (k !== 'label'))
            .filter(k => k !== 'vatRate')
            .map(k => { p('form Reset after filter', k); return k })
            .flatMap(k => assocPath([ id, k, 'error' ])('')))(model)
      }

      update(formReset(id))

      window.history.length > 1
        ? window.history.back()
        : window.location.assign(getUrl(page, params)) // hey
    },

    // handleSubmit :: (String, Object, Object, String) -> void 0
    handleSubmit: function (id, accounts, commission, token) {
      return function (ev) {
        const data =
          Object
            .values(ev.target.elements)
            .reduce((acc, el) => {
              let key = el.name
              let value = el.files != null ? el.files[0] : el.value
              if (key) acc[key] = value
              return acc
            }, {})
        p('[handleSubmit :: action] data: ', data)
        const validated = validateSettlementForm(data, commission.settleableTotal)
        const errs =
          Object
            .values(validated)
            .map(v => v.error)
            .filter(e => e !== '')

        p('[handle Submit :: action]', 'update validated form Data')
        update(mergeDeepLeft({ [id]: validated }))

        if (errs.length !== 0) {
          p('[handle Submit :: action]', 'form errors:', errs.length, errs)
          return
        }

        p('[handle Submit :: action]', 'no form validation errs', 'processing settlment now...')
        parentActions
          .processSettlement(validated, commission, accounts, token)
      }
    }
  }
}

const createModel = () => {
  const formField = (defaultVal = '') => ({ value: defaultVal, error: '' })
  return {
    sender: formField(),
    recipient: formField(),
    settlementLedger: formField(),
    base: formField(),
    vatRate: formField(0.19),
    vatValue: formField(),
    value: formField(),
    rxDate: formField(new Date().toISOString().split('T')[0]),
    rxNumber: formField(),
    isPayable: formField(false), // boolean, false for invoice,
    termsConfirmed: formField(false),
    attachment: formField()
  }
}

const createView = ({ id = 'settlement' }) => ({ actions, settleable }) => {
  return function SettlementForm (state) {
    const {
      status, // FIXME to _status
      isPayable,
      rxNumber,
      rxDate,
      base,
      vatRate,
      vatValue,
      value,
      attachment,
      termsConfirmed
    } = state[id]
    const { accounts, auth } = state
    const commission = computeCommission(accounts)
    const { settleableTotal } = commission
    const confirmTxt = 'Ich bestätige, dass die von mir im Abrechnungsformular eingetragenen Informationen, mit den Angaben auf der Originalrechnung exakt übereinstimmen.'

    return ['div',

      ['section',
        { className: css`max-width: 48rem`.class },
        settleable(state)
      ],

      ['form',
        { noValidate: true,
          id: 'myForm',
          name: 'myForm',
          onSubmit: compose(
            actions.handleSubmit(id, accounts, commission, auth.token),
            preventDefault
          )
        },

        // flex container
        ['div' + css`
          display: flex;
          flex-direction: column;`
          .$large`
            flex-direction: row;
            justify-content: start`,

          // form-group
          ['div' + css`width: 100%; max-width: 20rem`,
            ['h6' + css`
              font-size: ${theme.fontSizes[1]};
              color: ${theme.colors.black60};
              margin-top: ${theme.space[5]}`,
              'Rechnungsdaten'],

            ['input',
              { type: 'text',
                name: 'sender',
                hidden: true,
                readOnly: true,
                value: path([ UserAccount, 'address' ])(accounts) }],

            ['input',
              { type: 'text',
                name: 'recipient',
                hidden: true,
                readOnly: true,
                value: path([ DebitorAccount, 'address' ])(accounts) }],

            ['select',
              { name: 'isPayable',
                hidden: true,
                readOnly: true,
                defaultValue: typeof isPayable.value === 'boolean' ? isPayable.value : Boolean(false),
                // defaultTo(false)(path([ 'isPayable', 'value' ])(form)),
                onChange: actions.toggleIsPayable(id, isPayable.error) },
              ['option', { label: 'Rechnung', value: false }] // 'invoic'
              // ['option', { label: 'Zahlungsavis', value: true }] // 'remadv'
            ],

            // [Label, { htmlFor: 'recipient' }, 'Empfänger'],
            ['input',
              { name: 'recipient',
                type: 'text',
                hidden: true,
                readOnly: true,
                value: path([ DebitorAccount, 'address' ])(accounts),
                size: path([ DebitorAccount, 'address' ])(accounts).length,
                className: 'truncate courier' }],
              // form.recipient.error && [ErrLabel, {}, form.recipient.error]

            [InputGroup, { htmlFor: 'rxNumber' },
              [Label, { htmlFor: 'rxNumber' }, 'Rechnungsnummer'],
              [Input,
                { type: 'text',
                  name: 'rxNumber',
                  placeholder: '',
                  css: `font-family: Courier Next,courier,monospace;`,
                  required: true,
                  defaultValue: rxNumber.value || '',
                  error: rxNumber.error,
                  onChange: actions.setRxNumber(id, rxNumber.error)
                }],
              rxNumber.error && [Hint, { error: rxNumber.error }]
            ],

            [InputGroup, { htmlFor: 'rxDate' },
              [Label, { htmlFor: 'rxDate' }, 'Rechnungsdatum'],
              [Input,
                { type: 'date',
                  name: 'rxDate',
                  css: `font-family: Courier Next,courier,monospace;`,
                  style: { maxHeight: '2.25rem' },
                  lang: 'de',
                  required: true,
                  defaultValue:
                    new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000)
                      .toISOString()
                      .split('T')[0],
                  error: rxDate.error,
                  onChange: actions.setRxDate(id, rxDate.error) }
              ],
              rxDate.error && [Hint, { error: rxDate.error }]
            ],

            [InputGroup, { htmlFor: 'base' },
              [Label, { htmlFor: 'base' }, 'Provision (netto)'],
              [Input,
                { type: 'number',
                  name: 'base',
                  min: 0.01,
                  // max: furyTo$(grossToNet(settleableTotal, Number(vatRate.value))),
                  placeholder:
                    furyTo$(grossToNet(settleableTotal, Number(vatRate.value)))
                      .toLocaleString('de-DE'),
                  step: 'any',
                  lang: 'de',
                  css: 'font-family: Courier Next,courier,monospace;',
                  required: true,
                  value: base.value ? Number(base.value) : '',
                  error: base.error,
                  onChange: actions.setBase(id, base.error, settleableTotal)
                }],
              [Suffix, 'EUR'],
              base.error && [Hint, { error: base.error }]
            ],

            [InputGroup, { htmlFor: 'vatRate' },

              [Label, { htmlFor: 'vatRate' }, 'zuzügl. Umsatzsteuer'],

              [Prefix,
                [Select,
                  { name: 'vatRate',
                    type: 'number',
                    min: 0,
                    max: 1,
                    defaultValue: vatRate.value ? Number(vatRate.value) : 0.19,
                    onChange: actions.setVatRate(id, vatRate.error) },
                  [Option, { label: '7 %', value: 0.07 }, '7 %'],
                  [Option, { label: '19 %', value: 0.19 }, '19 %']
                ]
              ],

              [Input,
                { name: 'vatValue',
                  type: 'number',
                  readOnly: true,
                  css: `
                    font-family: Courier Next,courier,monospace;
                    text-align: right;`,
                  lang: 'de',
                  min: 0,
                  max: vatValue.value || 0,
                  required: true,
                  value: vatValue.value
                    ? Number(vatValue.value) // .toLocaleString('de-DE')
                    : 0,
                  error: vatRate.error
                }
              ],

              [Suffix, 'EUR'],

              vatRate.error && [Hint, { error: vatRate.error }]
            ],

            [InputGroup, { htmlFor: 'value' },
              [Label, { htmlFor: 'value' }, 'Summe (brutto)'],
              [Input,
                { type: 'number',
                  name: 'value',
                  label: 'Summe (brutto)',
                  min: 0.01,
                  max: furyTo$(settleableTotal),
                  placeholder: furyTo$(settleableTotal).toLocaleString('de-DE'),
                  step: 'any',
                  lang: 'de',
                  css: 'font-family: Courier Next,courier,monospace;',
                  required: true,
                  value: value.value ? Number(value.value) : '',
                  error: value.error,
                  onChange: actions.setValue(id, value.error, settleableTotal) }
              ],
              [Suffix, 'EUR'],
              value.error && [Hint, { error: value.error }]
            ]
          ],

          ['div', // form-group
            ['h6' + css`
              font-size: ${theme.fontSizes[1]};
              color: ${theme.colors.black60};
              margin-top: ${theme.space[5]}`,
                'Originalrechnung als Dateianhang'],

            [InputGroup, { htmlFor: 'attachment' },
              [Label, { htmlFor: 'attachment' }, 'Datei'],
              [Input,
                { type: 'file',
                  name: 'attachment',
                  accept: 'application/pdf, application/msword, image/*',
                  required: true,
                  css: 'font-family: Courier Next,courier,monospace;',
                  error: attachment.error,
                  onChange: actions.setAttachment(id, attachment.error) }
              ],
              attachment.error && [Hint, { error: attachment.error }]
            ],

            ['div' + css`
              display: flex;
              flex-direction: row;
              flex-no-wrap: noarg;
              // items-align: baseline;
              margin-top: ${theme.space[4]}`,

              [Input,
                { type: 'checkbox',
                  name: 'termsConfirmed',
                  required: true,
                  value: termsConfirmed.value,
                  error: termsConfirmed.error,
                  onChange: actions.setConfirmation(id, termsConfirmed.error) }
              ],

              ['label' + css`
                font-size: ${theme.fontSizes[1]};
                margin-left: ${theme.space[2]}
                padding-left: ${theme.space[2]};
                text-measure`,
                { htmlFor: 'termsConfirmed' },
                confirmTxt
              ],

              termsConfirmed.error && [Hint, { error: termsConfirmed.error }]
            ]
          ]
        ],

        status // .b.f6.mt5'
          ? ['div' + css`
              font-size: ${theme.fontSizes[1]};
              font-weight: 600;
              margin-top: ${theme.space[5]}`,
            'Status: ' + status]

          : ['div' + css`margin-top: ${theme.space[5]}`,
            [Button,
              { // className: 'b--blue b ba br2 bg-transparent blue input-reset ph3 pv2' + (status ? ' o-30' : ' dim'),
                color: 'blue',
                disabled: status !== '',
                type: 'submit' },
              'Senden'
            ],
            ['a' + css`
              font-size: ${theme.fontSizes[1]};
              margin-left: ${theme.space[3]};
              hover-pointer`,
              { onClick: actions.navigateBack(id) },
              'Abbrechen'
            ]
          ]
      ]
    ]
  }
}

export const createSettlementForm = (config = {}) => ({ parentActions, getState, update }) => {
  const settleable = createSettleable().view
  const actions = createActions({ update, getState, parentActions })

  return {
    model: () => createModel(),
    view: createView(config)({ actions, settleable })
  }
}

export const SettlementForm = (config) => ({
  dependencies: {
    settleable: Settleable
  },
  actions: createActions,
  model: () => createModel(),
  view: createView(config)
})
