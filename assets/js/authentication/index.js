import { tap, pipe, mergeDeepRight } from '../util/fp'

import * as furyRpc from '../data/furyRpc'
import localStore from '../data/localStore'
import serverApi from '../data/serverApi'

import { createCredentials, Credentials } from '../credentials/index'

import { createActions } from './actions'
import { createNextAction } from './nextAction'
import { createView } from './view'

function createModel (credentials) {
  const seed = {
    username: {},
    salesPartnerId: {},
    authenticated: false,
    message: '',
    status: '',
    token: ''
  }

  return { auth: mergeDeepRight(seed, credentials.model()) }
}

export const createSignIn = ({ update }) => {
  const actions = createActions({ serverApi, localStore })({ update })
  const Credentials = createCredentials({ id: 'auth' })({ parentActions: actions, update })
  return {
    model: () => createModel(Credentials),
    view: pipe(
      tap(createNextAction(update)),
      createView({ actions, credentials: Credentials.view })
    )
  }
}

const dependencies = { credentials: Credentials({ id: 'auth' }) }
export const Authentication = {
  dependencies,
  actions: createActions({ serverApi, localStore }),
  model: () => createModel(dependencies.credentials),
  nextAction: createNextAction({ furyRpc, localStore, serverApi }),
  view: createView
}
