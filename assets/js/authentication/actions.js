import { p } from '../util/debug'
import { compose } from '../util/fp'

import { authFail, authStatus, authMessage } from './updates'

export const createActions = ({ serverApi, localStore }) => ({ update }) => {
  function submitSignInRequest (validatedCreds, callbackUrl = window.location.href) {
    const errors = Object.values(validatedCreds).filter(({ error }) => error !== '')
    p('[submitSignInRequest :: action]', 'validated credentials', validatedCreds)
    if (errors.length > 0) return

    p('[submitSignInRequest :: action]', 'set state.auth.message to "Sending login mail..."')
    update(compose(
      authStatus('in_progress'),
      authMessage('Sending login mail...')
    ))

    const username = validatedCreds.username.value
    const salesPartnerId = validatedCreds.salesPartnerId.value
    serverApi
      .submitCredentials({ username, salesPartnerId, callbackUrl })
      .then(res => {
        // p('[submitSignInRequest :: action]','Server response', res)
        p('[submitSignInRequest :: action]', 'set state.auth.authStatus to "e_mail_sent"')
        update(compose(
          authMessage(res.data.toString()),
          authStatus('e_mail_sent')
        ))
      }, err => { serverApi.logError(err); throw err })
      .then(() => {
        p('[submitSignInRequest :: action]', 'persist auth in local storage')
        localStore
          .persistAuth(validatedCreds)
          .catch(err => console.error(err))
      })
      .catch(err => {
        let msg
        if (err.response) {
          msg = err.response.data.message
        } else if (err.request) {
          msg = 'Server did not respond.'
        } else {
          msg = err.message
        }
        update(authFail('Authentication failed :(   ' + msg, err))
      })
  }

  function signOutUser () {
    p('[signOutUser :: action]', 'sign out user')
    update(authFail('You Signed out'))
    localStore
      .clearAuth()
      .catch(err => console.error(err))
  }

  return {
    submitSignInRequest,
    signOutUser
  }
}
