import { HomePage, getUrl } from '../util/router'
import { Button, Notification, Spinner, Title } from '../common'
import { PageLayout } from '../layout'

export const createView = ({ actions, credentials }) => {
  function SignIn (state) {
    const { authenticated, message, status, username } = state.auth

    if (status === 'in_progress') {
      return [Spinner, {}, message || 'Authenticating...']
    }

    if (status === 'e_mail_sent') {
      return [PageLayout,
        [Notification,
          { type: 'fullscreen' },
          [Title, {}, 'You got mail.'],
          ['p', message || 'A log-in link was sent to your e-mail address.'],
          ['p',
            ['span', 'To sign into your account, please click on the link in the email.'],
            ['br'],
            ['span', 'You can close this window.']
          ]
        ]
      ]
    }

    return [PageLayout,
      { size: 'login' },

      [Title, !authenticated // && status !== 'authenticated'
        ? 'Please sign in'
        : `Hello ${username.value},`
      ],
      message && ['p', message],
      !authenticated // && status !== 'authenticated'
        ? credentials(state)
        : [['p', 'You are signed in'],
          ['div',
            [Button,
              { onClick: actions.signOutUser },
              'Sign out'],

            ['a.b.black-70.f6.hover-blue.link.ml3',
              { href: getUrl(HomePage) },
              'Back to home page']
          ]
        ]

    ]
  }

  return SignIn
}
