import { settlementLedger } from '../util/constants'
import { p } from '../util/debug'
import { assocPath, compose, dissocPath, mergeDeepLeft } from '../util/fp'
import {
  DebitorAccount,
  EntitlementAccount,
  IncomeAccount,
  SettlementsAccount,
  SignInPage,
  UserAccount
} from '../util/router'

import {
  validateSalesPartnerId,
  validateToken,
  validateTokenPayload
} from '../validation/credentials'

/*
  updates
 */

import {
  authFail,
  authMessage,
  authStatus,
  navigateToUrl
} from './updates'

function validateJwt (token) {
  const validatedToken = validateToken(token)
  const validatedPayload = validateTokenPayload(token)
  const errs =
    [ validatedToken, validatedPayload ]
      .filter(({ error }) => error !== '')
      .map(({ error }) => error)

  return !errs
    ? { validatedPayload, validatedToken }
    : { validatedPayload, validatedToken, errors: errs }
}
/*

  Next Action Predicate

 */

export const createNextAction = ({ furyRpc, localStore, serverApi }) => (update) => {
  /* helper fns */

  function fetchId (jwt, address) {
    return serverApi
      .fetchSalesPartnerId(jwt, address)
      .then(res => {
        return validateSalesPartnerId(res.data)
      }, err => {
        serverApi.logError(err)
        throw err
      })
  }

  // generateUserNode :: (Object, Object) -> Object
  function generateUserNode (tokenPayload, credentials) {
    console.assert(tokenPayload.value.extid === credentials.username.value,
      '[generateUserNode :: nextAction] assert validatedPayload.value.extid === credentials.username.value')
    const extid = credentials.username.value
    const user = {
      extid,
      privateKey: // FIXME unsecure !!!
        furyRpc.utils.decrypt(tokenPayload.value.salesPartnerKey, credentials.salesPartnerId.value)
    }
    const nodeFn = function () { return furyRpc.createNode(user) }

    return { user, nodeFn }
  }

  // fetchAccounts :: (Object, Object) -> Object
  function fetchAccounts (user, node) {
    const { extid } = user
    const { address } = node.wallet
    const debitor = {}
    const accounts = {}
    return Promise
      .all([
        furyRpc.getRelation(address, 41, node), // provider Ledger (SEV ledger) (debitor)
        furyRpc.fetchLedger(node, true), // sales Partner ledger or create one (creditor)
        furyRpc.getRelation(address, 81, node) // get creditor entitlement account address
      ])
      .then(res => {
        const debitorLedger = res[0].toLowerCase() // this should not be necessary!
        const userAddress = address.toLowerCase()
        const userLedger = res[1].toLowerCase()
        const entitlementAddress = res[2].toLowerCase()
        debitor.ledger = debitorLedger
        accounts[UserAccount] = { address: userAddress, ledger: userLedger, extid, label: 'Reseller Account' }
        accounts[DebitorAccount] = { ledger: userLedger, label: 'Stadtwerke Energieverbund' }
        accounts[EntitlementAccount] = { address: entitlementAddress, ledger: debitorLedger, label: 'Provisionsanspruch' }
        accounts[SettlementsAccount] = { address: userAddress, ledger: settlementLedger, label: 'Provisionsabrechnungen' }
        accounts[IncomeAccount] = { address: userAddress, ledger: debitorLedger, label: 'Provisionskonto' }
        return furyRpc.fetchLedgerOwner(res[0], { n: node }) // fetch provider address (debitor)
      })
      .then(res => {
        debitor.address = res.toLowerCase()
        debitor.label = 'SEV Stadtwerke Energieverbund' // hardcoded for now
        accounts[DebitorAccount].address = debitor.address
        const accountIds = Object.keys(accounts)

        return { debitor, accounts, accountIds }
      })
  }

  function authenticate (user = {}) {
    return {
      with: function (credentials, token = '', node = {}) {
        return serverApi.checkAuth(token)
          .then(() => fetchAccounts(user, node), err => {
            serverApi.logError(err)
            throw err
          })
          .then(({ debitor, accounts, accountIds }) => ({
            accounts,
            accountIds,
            auth: {
              authenticated: true,
              message: '',
              status: 'idle',
              token,
              salesPartnerId: credentials.salesPartnerId,
              username: credentials.username
            },
            debitor,
            user
          }))
      }
    }
  }

  function persistAuthenticated ({ accounts, accountIds, auth, debitor, user }) {
    return serverApi
      .perpetuateAuth(auth.token)
      .then(res => {
        const permToken = validateToken(res.data)
        auth.token = permToken.error ? auth.token : permToken.value

        Promise.all([
          localStore.persistAuth(auth),
          localStore.persistAppState({ debitor, user, accounts, accountIds })
        ]).catch(err => console.error('[persistAuthenticated :: service :: authentication]', err))

        return { accounts, accountIds, auth, debitor, user }
      }, err => {
        serverApi.logError(err)
        return { accounts, accountIds, auth, debitor, user }
      })
  }

  /* Next Action */

  return function Authenticator (state) {
    if (state.pageId !== SignInPage || !state.params.token) return

    p('[authenticator :: service :: authentication]', 'is firing')
    const { auth, params } = state
    const { authenticated, status, username, salesPartnerId } = auth
    const token = params.token

    if (authenticated) {
      p('[authenticator :: service :: authentication]', `calling actions.redirectToDest with state update`)
      update(compose(
        dissocPath(['params', 'token']),
        dissocPath(['params', 'authredirect']),
        authStatus(''),
        authMessage(''),
        navigateToUrl(params.authredirect)
      ))
      return
    }

    if (status !== 'in_progress') {
      p('[authenticator :: nextAction]', 'set state.auth.status to "in_progress"')
      update(authStatus('in_progress'))
      return
    }

    const {
      validatedToken,
      validatedPayload,
      errors
    } = validateJwt(token)
    if (errors.length !== 0) {
      // p('[authenticator :: nextAction]', 'jwt errors', errors)
      let msg = 'Authentication failed. Token not valid. Please try again or contact someone.'
      console.error('[authenticator :: nextAction]', msg)
      update(authFail(msg, errors[0]))
      return
    }

    p(
      '[authenticator :: nextAction]', 'Token payload ok:',
      Math.round(((new Date().getTime() / 1000) - validatedPayload.value.iat) / 120) + ' min since auth'
    )

    const validatedId = validateSalesPartnerId(salesPartnerId.value)
    if (validatedId.error) {
      // p([[authenticator :: nextAction]], validatedId, validatedPayload, validatedToken)
      console.warn('[authenticator :: nextAction]', 'salesPartnerId should exist, but is:', validatedId.error)
      console.info('[authenticator :: nextAction]', 'Query salesPartnerId from server as fallback')
      fetchId(validatedToken.value, validatedPayload.value.salesPartnerAddress)
        .then(({ value, error }) => {
          if (error) throw new Error(error)
          p('[authenticator :: nextAction]', 'update validated salesPartnerId', '{ value: ' + value, 'error: ' + error + ' }')
          update(assocPath(['auth', 'salesPartnerId', 'value'])(value))
        })
        .catch(err => {
          let msg
          if (err.response) {
            msg = err.response.data.message
          } else if (err.request) {
            msg = 'Server did not respond.'
          } else {
            msg = err.message
          }
          console.error(err, msg)
          update(authFail('Authentication failed: ' + msg))
        })

      return
    }

    const validatedCreds =
      Object.assign({}, { username, salesPartnerId }, {
        username: {
          value: validatedPayload.value.extid,
          error: ''
        },
        salesPartnerId: {
          value: validatedId.value,
          error: ''
        }
      })

    p('[authenticator :: nextAction]', 'validated credentials: ', validatedCreds)

    const { user, nodeFn } = generateUserNode(validatedPayload, validatedCreds)
    const node = nodeFn()
    if (user.privateKey !== node.wallet.privateKey) {
      update(authFail(
        'Authentication failed.',
        new Error('[authenticator :: nextAction]' + ' Wallet keys dont match')
      ))
      return
    }

    authenticate(user)
      .with(validatedCreds, token, node)
      .then(persistAuthenticated)
      .then(toUpdate => {
        const {
          accounts,
          accountIds,
          auth, // { authenticated, message, status, token, salesPartnerId, username },
          debitor,
          user
        } = toUpdate
        p('[Authenticator :: nextAction]', 'update state.auth.token (permanent token)')
        update(compose(
          dissocPath(['params', 'token']),
          dissocPath(['params', 'authredirect']),
          mergeDeepLeft({ accounts, accountIds, auth, debitor, user }),
          navigateToUrl(params.authredirect)
        ))
      })
      .catch(err => {
        console.error('[Authenticator :: nextAction]', 'Authentication failed', err)
        update(authFail('Authentication failed', err))
      })
  }
}
