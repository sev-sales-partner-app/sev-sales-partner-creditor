import { assocPath, compose, dissocPath } from '../util/fp'
import {
  SignInPage,
  defaultRoute,
  prefix,
  parseUrl,
  navigateTo
} from '../util/router'

// authMessage :: a -> Object -> Object
export const authMessage = assocPath([ 'auth', 'message' ])

// authStatus :: a -> Object -> Object
export const authStatus = assocPath([ 'auth', 'status' ])

// navigateToUrl :: String -> Object -> Object
export function navigateToUrl (href) {
  const route = href != null ? decodeURIComponent(href).split(prefix)[1] : defaultRoute
  return x => Object.assign({}, x, parseUrl(prefix + route))
}

// navigateToPage :: (String, Object?) -> Object -> Object
export function navigateToPage (pageId, params = {}) {
  return x => Object.assign({}, x, navigateTo(pageId, params))
}

// authFail :: (String, Error?) -> Function
export function authFail (message = 'Auth failed', err) {
  err && console.error('[authFail]', err)
  return compose(
    dissocPath([ 'params', 'token' ]),
    dissocPath([ 'auth', 'token' ]),
    assocPath([ 'auth', 'authenticated' ])(false),
    assocPath([ 'auth', 'salesPartnerId', 'value' ])(''),
    assocPath([ 'auth', 'username', 'value' ])(''),
    authStatus('idle'),
    authMessage(message),
    navigateToPage(SignInPage)
  )
}
