import { Title } from '../common'
import { PageLayout } from '../layout'

const createView = () => {
  return function Home () {
    return [PageLayout,
      [Title, 'Home Page Placeholder'],
      ['p', 'This is the home placeholder']
    ]
  }
}

// export const createHome = () => {
//   return {
//     view: createView()
//   }
// }

export const Home = { view: createView }
