// import { p } from '../util/debug'
import { blankHref } from '../util/constants'
import {
  HomePage,
  EntitlementAccount,
  IncomeAccount,
  SettlementPage,
  SettlementsAccount,
  getUrl
} from '../util/router'
import { b as css, createElement as el, theme } from '../util/view'
import { Container } from './container'
import { Rim } from './rim'

const navItems = [
  // { id: HomePage, name: 'Home' },
  // { id: SignInPage, name: 'Log in' },
  // { id: UserAccount, name: 'Account' },
  { id: EntitlementAccount, name: 'Provisionsanspruch' },
  { id: SettlementsAccount, name: 'Abrechnungen', filter: 'sent' },
  { id: IncomeAccount, name: 'Einnahmen' }
]

function getLabel (state) {
  let res
  try {
    res = state.pageId === SettlementPage
      ? state[SettlementPage].label
      : state.accounts[state.pageId].label || state.pageId
  } catch (err) {
    res = state.pageId
  }
  return res
}

const createView = () => {
  function Header (state) {
    return ['header.w-100.pa3.ph5-ns.bg-white',
      ['div.db.dt-ns.mw9.center.w-100',

        ['div.db.dtc-ns.v-mid.tl.w-50',
          ['a',
            { className: 'dib f5 f4-ns fw5 mt0 mb1 link black-70',
              title: 'Home',
              href: getUrl(HomePage) },
            ['span', {}, 'SEV ',
              ['small.nowrap.fw2', { }, 'Sales Partner' + ' ' + getLabel(state)]
            ]
          ]
        ],

        ['nav', { className: 'db dtc-ns v-mid w-100 tl tr-ns mt2 mt0-ns' },
          navItems.map((item, i) =>
            ['a',
              { key: i,
                className: 'f6 fw6 hover-blue link mr2 mr3-m mr4-l dib ' +
                    (state.pageId === item.id ? 'blue' : 'black-70'),
                href: item.id
                  ? getUrl(item.id, item.filter ? { filter: item.filter } : {})
                  : blankHref },
              item.name]
          )
        ]
      ]
    ]
  }

  return Header
}

export const createHeader = () => {
  return {
    view: createView()
  }
}

export const Header = {
  view: createView
}

// NEW HEADER HERE //////

const { colors, fontSizes, space } = theme

export function HeaderNew ({ state }) {
  return (
    el(Rim, { as: 'header' },
      el(Container,
        {
          // size: 'wide',
          css: css`
            display: flex;
            flex-direction: column;
            width: 100%;
            z-index: 1;`
            .$media('(min-width: 600px)', css`
              flex-align-items: baseline;
              flex-direction: row;
              flex-wrap: wrap;`)
        },

        // Header Brand
        el('div',
          { className: [ 'Brand', css`margin-right: auto;` ].join(' ') },
          el('a',
            {
              title: 'Home',
              href: getUrl(HomePage),
              className: css`
                color: ${colors.black70};
                display: block;
                font-size: ${fontSizes[2]};
                font-weight: 500;
                min-height: 56px;
                padding: 16px 0;`
                .$notSmall(css`font-size: ${fontSizes[3]};`)
                .link
                .class
            },
            'SEV',
            el('small',
              {
                className: css`
                  font-weight: 200;
                  white-space: nowrap;`
                  .class
              },
              ' Sales Partner' + ' ' + getLabel(state)
            )
          )
        ),

        // Header Nav
        el('nav',
          {
            className: css`
              display: flex;
              flex-direction: column;
              margin-top: ${space[0]};`
              .$media('(min-width: 600px)', css`
                flex-direction: row;
                flex-wrap: wrap;`)
              .class
          },
          navItems.map(item =>
            el('a',
              {
                key: item.id,
                href: item.id
                  ? getUrl(item.id, item.filter ? { filter: item.filter } : {})
                  : blankHref,
                className: css`
                  font-size: ${fontSizes[1]};
                  font-weight: 600;
                  padding: ${space[2]} 0;
                  margin-top: ${space[1]};
                  color: ${state.pageId === item.id ? colors.blue : colors.black70};
                  hover-color: ${colors.blue};`
                  .$media('(min-width: 600px)', css`
                    padding: ${space[3]} 0;
                    margin: 0 ${space[3]};`)
                  .$firstChild('margin-left: 0;')
                  .$lastChild('margin-right: 0;')
                  .link
                  .class
              },
              item.name
            )
          )
        )
      )
    )
  )
}
