import { b as css, createElement as el } from '../util/view'

export function Main (props) {
  const style = css`flex: 1 0 auto;`

  return (
    el('main',
      { className: [ 'Layout main', style.class ].join(' ') },
      props.children
    )
  )
}
