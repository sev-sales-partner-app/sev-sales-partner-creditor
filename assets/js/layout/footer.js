import { b as css, createElement as el, theme } from '../util/view'
import { Container } from './container'
import { Rim } from './rim'
import { getUrl, SignInPage } from '../util/router'

const createView = () => {
  function Footer (state) {
    const { consensus } = state
    return ['footer..b--black-10.db.black-60.f6.ph3.ph5-ns.pv3.pt4-ns',
      ['span',
        `SEV #Block ${consensus.block} @ ${new Date(consensus.time * 1000).toLocaleTimeString('de-De')}`
      ]
    ]
  }

  return Footer
}
export const createFooter = () => {
  return {
    view: createView()
  }
}

export const Footer = {
  view: createView
}

// NEW FOOTER HERE //////

const { colors, fontSizes, space } = theme

export function FooterNew ({ state }) {
  const sticky = css`flex-shrink: 0`

  const style = css`
    border-color: ${colors.black10};
    color: ${colors.black60};
    display: flex;
    font-size: ${fontSizes[0]};
    flex-direction: column;
    line-height: 1.5em;
    padding: ${space[3]} ${space[0]}
  `['$large'](css`
    flex-direction: row;
    justify-content: space-between;
`)

  const { block, time } = state.consensus

  return (
    el(Rim,
      { as: 'footer',
        css: sticky },

      el(Container,
        { css: style }, // size: 'wide'
        el('samp',
          {},
          `#Block ${block} @ ${new Date(time * 1000).toLocaleString('de-De')}`
        ),

        state.auth.authenticated &&
          el('samp', null,
            el('a',
              {
                className: css`
                  color: ${colors.black60};
                  hover-color: ${colors.blue}
                  hover-underline: noarg;
                  link: noarg;`['class'],

                href: getUrl(SignInPage)
              },
              state.user.extid)
          )
      )
    )
  )
}
