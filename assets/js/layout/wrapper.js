import { b as css, createElement as el, theme } from '../util/view'

export function Wrapper (props) {
  const style = css`
    color: ${theme.colors.black80};
    display: flex;
    flex-direction: column;
    height: 100vh;`

  return (
    el('div',
      { className: [ 'Layout wrapper', style.class ].join(' ') },
      props.children
    )
  )
}
