import flyd from 'flyd'

import { pipe } from './util/fp'
import { render } from './util/view'
import { createAppALT as createApp } from './app'

const update = flyd.stream()
const getState = flyd.stream()

createApp(update, getState).then(app => {
  // console.log('DEBUG wired App', app)
  const T = (x, f) => f(x)
  const models = flyd.scan(T, app.model, update)
  const states = models.map(app.state) // sync app.state fn

  // const states = flyd.stream() // async app.state fn
  // models.map(model => { app.state(model).then(states) })

  states.map(pipe(app.view, render(document.getElementById('app'))))
  states.map(app.nextAction)
  getState.map(f => f(states()))

  if (process.env.NODE_ENV !== 'production') {
    require('meiosis-tracer')({
      streams: [
        { stream: models, label: 'models' },
        { stream: states, label: 'states' }
      ]
    })
  }

  // return { models, states, update }
})

/*

// with out initial state promise
const App = createApp(update)
const models = flyd.scan((x, f) => f(x), App.model(), update)
const states = flyd.stream()

models.map(model => {
  App.state(model).then(states)
})
states.map(pipe(App.view, render(document.getElementById('app'))))
states.map(App.nap)

export { models, states, update }

*/
