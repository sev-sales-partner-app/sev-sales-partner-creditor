import { p } from '../../util/debug'
import { assoc, assocPath, compose, identity as I, match, tap } from '../../util/fp'
import { removeUrlParameter } from '../../util/index'
import { prefix, SignInPage, defaultRoute, getUrl } from '../../util/router'

export const authIntercept = {
  state: function (model) {
    // location intercept
    // const rqHref = window.location.href
    // const rqRoute = rqHref.split(prefix)[1] || defaultRoute
    const params = model.params // || decodeURLParams(rqRoute.split('?')[1] || '')

    // redirect param
    const parseHref = compose(
      encodeURIComponent,
      href => removeUrlParameter(href, 'token'),
      href => removeUrlParameter(href, 'authredirect')
    )
    const authredirect =
        parseHref(window.location.origin + '/' + (getUrl(model.pageId, params) || prefix + defaultRoute))

    // Assertions
    const isAuthenticated = model.auth.authenticated === true && !params.token // model.auth.status === 'authenticated'
    const isSignInPage = model.pageId === SignInPage

    function pass () { return isAuthenticated } // model.auth.status === 'authenticated'
    function token () { return 'token' in params || (!isAuthenticated && model.auth.token) }
    function intercept () { return !isSignInPage }

    // Actions
    const logOk = tap(() => p('[authIntercept :: service]', 'All good, you are signed in'))
    const logToken = tap(() => p('[authIntercept :: service]', 'found token'))
    const logIntercept = tap(() =>
      p('[authIntercept :: service]',
        `Not authenticated. redirecting from ${window.location.href} to ${getUrl(SignInPage, { authredirect })}...`)
    )
    const redirectToSignIn = compose(
      assoc('url')(getUrl(SignInPage, { authredirect })),
      assoc('params')({ authredirect }),
      assoc('pageId')(SignInPage),
      logIntercept
    )

    const stayOnSignInPage =
        !isAuthenticated && isSignInPage
          ? I
          : redirectToSignIn

    const proccessToken = compose(
      assocPath(['auth', 'status'])('in_progress'),
      stayOnSignInPage,
      logToken
    )

    return match(model)
      .on(pass, logOk)
      .on(token, proccessToken)
      .on(intercept, redirectToSignIn)
      .otherwise(stayOnSignInPage)
  },

  initial: null // (seed) => (Object.assign({}, seed, {})
}
