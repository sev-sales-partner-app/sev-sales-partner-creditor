import { accountQuery } from './accountQuery'
// import { randomNumService } from './randomNum'
import { authIntercept } from './authIntercept'
// import { consensusLoader } from './consensusLoader'
import { browserLocationSync } from './browserLocation'

export const services = {
  // random number :: service
  // randomNumService,

  // auth redirect :: service
  authIntercept,

  // consensus loader :: service
  // consensusLoader,

  // customAccountQuery :: service
  accountQuery,

  // Update url in browser location bar :: service
  browserLocationSync
}
