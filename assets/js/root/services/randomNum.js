// random num service example

let memo = []
export const randomNumService = {
  state: function (model) {
    const RANDOMTEST = Math.round(Math.random() * 100000)
    memo.push(RANDOMTEST)
    return Object.assign({}, model, { RANDOMTEST })
  },

  initial: function () {
    return { RANDOMTEST: 0 }
  }
}
