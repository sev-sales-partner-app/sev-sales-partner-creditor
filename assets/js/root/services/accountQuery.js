import { p } from '../../util/debug'
import { assocPath, compose } from '../../util/fp'
import { QueriedAccount } from '../../util/constants'

export const accountQuery = {
  state: function (model) {
    const { params, pageId, accounts } = model
    const id = (params.ledger + '::' + params.address).toString()
    const account = accounts[id]

    const queried = [[
      account == null,
      params.address != null,
      params.ledger != null,
      pageId === QueriedAccount
    ]]
      .filter(conditions => conditions.every(Boolean))
      .map(res => {
        p('[accountQuery :: services]', 'handle account query')
        return res
      })[0]

    return !queried
      ? model
      : compose(
        assocPath([ 'accounts', id, 'address' ])(params.address),
        assocPath([ 'accounts', id, 'ledger' ])(params.ledger)
      )(model)
  },

  initial: null
}
