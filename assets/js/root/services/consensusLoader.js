import { p } from '../../util/debug'
import { assoc } from '../../util/fp'
import { now } from '../../util/index'

import * as furyRpc from '../../data/furyRpc'
import localStore from '../../data/localStore'

// createConsensusLoader :: (furyRpc, localStore) -> Object<{ state: Function, initial: Function }>
export const createConsensusLoader = ({ fetchCurrentBlockNumber }, { matchBlockTime }) => {
  let prevPage
  let prevCons
  return {
    // model -> Promise<state>
    state: function (model) {
      if ((prevCons != null) &&
        ((model.pageId === prevPage) || (prevCons.time + 19) > now())) {
        return assoc('consensus')(prevCons)(model)
      }
      prevPage = model.pageId
      prevCons = model.consensus
      p('[consensusLoader :: service]', 'retrieve current block number')
      return fetchCurrentBlockNumber().then(nblk => {
        return (prevCons != null && prevCons.block === nblk)
          ? assoc('consensus')(prevCons)(model)
          : matchBlockTime(nblk).then(({ block, time }) => {
            prevCons = { block, time }
            p('[consensusLoader :: service]', 'matched block time')
            return assoc('consensus')({ block, time })(model)
          })
      })
        .catch(err => console.error('[consensusLoader :: services]', err))
    },
    // initial :: () -> Object
    initial: function () {
      return { consensus: { block: 0, time: 0 } }
    }
  }
}

export const consensusLoader = createConsensusLoader(furyRpc, localStore)
