import { now } from '../util'
import {
  OxOO,
  settlementLedger,
  DebitorAccount,
  EntitlementAccount,
  IncomeAccount,
  SettlementsAccount
} from '../util/constants'
import { p } from '../util/debug'
import { assoc, assocPath, compose, defaultTo, head, isEmpty, prop } from '../util/fp'

// action helpers

function isTransferable (ledgerTye) {
  return ledgerTye === 'transferable'
}

// getAccountRefFrom :: Object -> Object -> Boolean?
const getAccountRefFrom = (accountsByRef = {}) => (account = {}) => {
  const { ledger, address } = account
  const res = Object.entries(accountsByRef).find(([k, v]) => {
    try {
      return v.ledger.toLowerCase() === ledger.toLowerCase() &&
        v.address.toLowerCase() === address.toLowerCase()
    } catch (e) {
      p(e)
      return v.ledger === ledger && v.address === address
    }
  }) || []
  return res[0]
}

// THE ACTIONS

export const createActions = ({ furyRpc, localStore }) => ({ update }) => {
  // retrieveTxsBlockTimes :: ({ data: Array, query: Array }) -> Promise<{ data: Array, query: Array }>
  function retrieveTxsBlockTimes ({ data = [], query = [] }) {
    return Promise
      .all(data.map(tx => localStore.matchBlockTime(tx.blockNumber)))
      .then(arr => ({
        data: data.flatMap((tx, i) => assoc('blockTime')(arr[i].time)(tx)),
        query
      }))
  }

  // handleRxsMsgs :: Object<{ data: Array?, query: Array? }> -> Promise<{ data: Array, query: Array }>
  function handleRxsMsgs ({ data = [], query = [] }, node) {
    return data.length === 0
      ? { data, query }
      : Promise
        .all(data.map(rx =>
          (rx.msg == null || rx.msg === OxOO) ? Promise.resolve('') : furyRpc.fetchStringStore(rx.msg, node)))
        .then(encStrgs => ({
          data: data.flatMap((rx, i) => assoc('message')(JSON.parse(window.atob(encStrgs[i])))(rx)),
          query
        }))
  }

  // fetchAccountsBxs :: (Object, Object) -> void 0
  function fetchAccountsBxs (accountsByRef = {}, opts) {
    const validEntry = ([ k, v ]) => head(k) !== '_' && v.address && v.ledger
    const shortlisted = ([ ref ]) => [
      DebitorAccount, EntitlementAccount, IncomeAccount
    ].map(x => x === ref).some(Boolean)

    p('[fetchAccountsBxs :: action]', 'set state.accounts._status to "fetching"')
    update(assocPath([ 'accounts', '_status' ])('fetching'))

    Promise.all(Object.entries(accountsByRef)
      .filter(validEntry)
      .filter(shortlisted)
      .map(([k, { address, ledger }]) => {
        return furyRpc
          .fetchAccountBalance(ledger, address,
            k !== SettlementsAccount ? opts : assoc('ledgerType')('transferable')(opts))
      }))
      .then(bxs => furyRpc.fetchCurrentBlockNumber(opts.n)
        .then(localStore.matchBlockTime)
        .then(consensus => ({ bxs, consensus })))
      .then(({ bxs, consensus }) => {
        p('[fetchAccountsBxs :: action]', 'update credit/debit values in state.accounts...')
        update(model => {
          return bxs.reduce((acc, bx) => {
            const { address, ledger, credit, debit } = bx
            const id = getAccountRefFrom(accountsByRef)(bx)
            console.assert(model.accounts[id].address === address && model.accounts[id].ledger === ledger,
              `[fetchAccountsBxs :: action] assertion:
                bx.address(${address}) should equal accounts[${id}].address (${model.accounts[id].address}) 
                --- AND --- 
                bx.ledger(${ledger}) should equal accounts[${id}].ledger (${model.accounts[id].ledger})`)
            return !id
              ? acc
              : compose(
                assocPath([ 'accounts', id, 'credit' ])(credit),
                assocPath([ 'accounts', id, 'debit' ])(debit)
              )(acc)
          }, compose(
            assocPath([ 'accounts', '_lastFetch' ])(consensus),
            assocPath([ 'accounts', '_status' ])('idle')
          )(model))
        })
      })
  }

  // maybeFetchBalances ::  (Object, Number) -> Boolean
  function maybeFetchBalances (accounts = {}, currentBlock) {
    const { _status, _lastFetch } = accounts
    return [ _lastFetch ]
      .map(lastFetch => defaultTo(0)(prop('block')(lastFetch)))
      .map(lastBlk => [ !isEmpty(accounts), _status !== 'fetching', lastBlk < currentBlock ])
      .filter(conditions => conditions.every(Boolean))
      .map(res => {
        p('[maybeFetchBalances :: action]', 'triggered fetchAccountsBxs fn, Reason:',
          `lastFetch (${defaultTo(0)(_lastFetch && _lastFetch.block)}) < currentBlock (${currentBlock})`)
        fetchAccountsBxs(accounts, { n: furyRpc.createNode() })
        return res
      })
      .length > 0
  }

  // fetchAccountTxs :: (String, Object, Object) -> void 0
  function fetchAccountTxs (id, account = {}, { threshold, maxBlocks, ledgerType = 'stromkonto', n } = {}) {
    const { address, ledger } = account
    const opts = { threshold, maxBlocks, ledgerType, n };

    [[ id, address, ledger, Number(maxBlocks) !== 0 ]].filter(c => c.every(Boolean)).map(() => {
      p('[fetchAccountTxs :: action]', `set state.accounts[${id}]._status to "fetching"`)
      update(assocPath([ 'accounts', id, '_status' ])('fetching'))

      Promise.all([
        furyRpc.fetchAccountBalance(ledger, address, opts),
        furyRpc.fetchDeepTxHistory(ledger, address, opts)
          .then(retrieveTxsBlockTimes)
          .then(isTransferable(opts.ledgerType) ? x => handleRxsMsgs(x, opts.n) : x => x)
      ])
        // .then(([ balance, txHistory ]) => {
        //   return localStore.matchBlockTime(txHistory.query[1])
        //     .then(consensus => [ balance, txHistory, consensus ])
        // })
        .then(([
          balance,
          txHist // ,
          // consensus
        ]) => {
          console.assert(account.ledger === balance.ledger && account.address === balance.address,
            `[fetchAccountTxs :: action] account keys in args ${account} and response ${balance} should be equal`)
          // let existingTxs
          // getState(state => {
          //   try {
          //     existingTxs = path('accounts', id, isTransferable(opts.ledgerType) ? 'rxs' : 'txs')(state) || []
          //   } catch (e) { existingTxs = [] }
          // })
          // const minBlock = existingTxs.map(tx => tx.blockNumber).reduce((acc, n) => {
          //   return n > acc ? n : acc
          // }, 0)
          // const finalList = txs.filter(tx => tx.blockNumber > minBlock).sort((a, b) => b.blockNumber - a.blockNumber)
          const { address, ledger, credit, debit } = balance
          const txs = txHist.data.reverse() // .concat(existingTxs)
          const rxs = isTransferable(ledgerType) ? txs : null
          const _lastFetch = { time: now(), blocks: txHist.query }
          const _status = 'idle'
          const res = rxs != null
            ? { address, ledger, credit, debit, rxs, _lastFetch, _status }
            : { address, ledger, credit, debit, txs, _lastFetch, _status }
          p('[fetchAccountTxs :: action]', `update state.accounts[${id}] and state.consensus`)
          update(compose(
            ...Object
              .entries(res)
              .map(([ k, v ]) => assocPath([ 'accounts', id, k ])(v))
            // assocPath([ 'consensus', 'time' ])(consensus.time),
            // assocPath([ 'consensus', 'block' ])(consensus.block)
          ))
        })
    })
  }

  // maybeFetchTransactions :: (String, Object, Number) -> Boolean
  function maybeFetchTransactions (id, account = {}, currentBlock) {
    const { address, ledger, _lastFetch, _status } = account

    return [ _lastFetch ]
      .map(lastFetch => defaultTo([ currentBlock, 0 ])(prop('blocks')(lastFetch))) // Maybe undefined
      .map(([fromBlock, toBlock]) => [
        !isEmpty(account),
        address,
        ledger,
        _status !== 'fetching',
        _status !== 'fetching_more',
        toBlock < currentBlock
      ])
      .filter(conditions => conditions.every(Boolean))
      .map(res => {
        p('[maybeFetchTransactions :: action]', 'triggered fetchAccountTxs fn, Reason:',
          `toBlock (${defaultTo(0)(_lastFetch && _lastFetch.blocks[1])}) < currentBlock (${currentBlock})`)
        fetchAccountTxs(id, { address, ledger }, {
          threshold: 1,
          ledgerType: ledger === settlementLedger ? 'transferable' : 'stromkonto',
          // maxBlocks: (_lastFetch && _lastFetch.query) ? ctx.consensus.block - _lastFetch.query[1] : undefined,
          n: furyRpc.createNode()
        })
        return res
      })
      .length > 0
  }

  function fetchMoreTxs (id, account = {}, { threshold, ledgerType = 'stromkonto' } = {}) {
    const { address, ledger } = account
    const opts = {
      threshold,
      ledgerType,
      n: furyRpc.createNode()
    };

    [[ id, address, ledger ]].filter(conditions => conditions.every(Boolean)).map(() => {
      p('[fetchMoreTxs :: action]', 'set account._status to "fetching_more"')
      update(assocPath([ 'accounts', id, '_status' ])('fetching_more'))

      furyRpc.fetchDeepTxHistory(ledger, address, opts)
        .then(retrieveTxsBlockTimes)
        .then(isTransferable(opts.ledgerType) ? x => handleRxsMsgs(x, opts.n) : x => x)
        .then((res) => {
          p('[fetchMoreTxs :: action]', `update state.accounts[${id}].${isTransferable(opts.ledgerType) ? 'rxs' : 'txs'}`)
          const txs = res.data.reverse()
          const rxs = isTransferable(ledgerType) ? txs : null
          const _lastFetch = { time: now(), blocks: res.query }
          const _status = 'idle'
          update(compose(
            ...Object
              .entries(rxs ? { rxs, _lastFetch, _status } : { txs, _lastFetch, _status })
              .map(([ k, v ]) => assocPath([ 'accounts', id, k ])(v))
          ))
        })
    })
  }

  // maybeFetchMoreTransactions :: (String, Object, Object, Number) -> Boolean
  function maybeFetchMoreTransactions (id, account = {}, tx = {}, currentBlock) {
    const { address, ledger, _lastFetch, _status } = account
    const xs = account.rxs || account.rxs || []
    return [ _lastFetch ]
      .map(lastFetch => defaultTo([ currentBlock, 0 ])(prop('blocks')(lastFetch))) // Maybe undefined
      .map(([ fromBlock ]) => [
        !isEmpty(account),
        _status !== 'fetching',
        _status !== 'fetching_more',
        fromBlock > 0,
        isEmpty(tx)
      ])
      .filter(conditions => conditions.every(Boolean))
      .map(res => {
        const opts = {
          threshold: isEmpty(xs) ? 4 : xs.length * 2,
          ledgerType: 'transferable', // account.ref === SettlementsAccount ? 'transferable' : 'stromkonto',
          n: furyRpc.createNode()
        }
        p('[ :: settlements]', 'triggered fetchMoreTxs fn')
        p('[maybeFetchMoreTransactions :: action]',
          `triggered ${account.debit == null || account.credit == null ? 'fetchAccountTxs' : 'fetchMoreTxs'} fn, Reason:`,
          `(1) fromBlock is not zero -> ${defaultTo(currentBlock)(_lastFetch && _lastFetch.blocks[0])}`,
          `(2) settlemet not found in state.accounts[${id}]`)
        account.debit == null || account.credit == null
          ? fetchAccountTxs(id, { address, ledger }, opts)
          : fetchMoreTxs(id, { address, ledger }, opts)
        return res
      })
      .length > 0
  }

  return {
    // account actions
    maybeFetchBalances,
    maybeFetchTransactions,
    maybeFetchMoreTransactions,
    fetchMoreTxs
  }
}
