// import { p } from '../util/debug'
import { mergeDeepRight } from '../util/fp'
import {
  HomePage,
  SettlementsAccount,
  UserAccount,
  DebitorAccount,
  EntitlementAccount,
  IncomeAccount,
  QueriedAccount,
  SignInPage,
  SettlementPage
} from '../util/constants'

import * as furyRpc from '../data/furyRpc'
import localStore from '../data/localStore'

import { createHeader, Header } from '../layout/header'
import { createFooter, Footer } from '../layout/footer'

import { Wrapper, Main, FooterNew, HeaderNew } from '../layout'

import { createNotFound, NotFound } from '../notFound'
// import { Home } from '../home'
import { createSignIn, Authentication } from '../authentication'
import { createAccount, Account } from '../account'
import { createSettlement, Settlement } from '../settlement'

import { createActions } from './actions'
import { createNextAction } from './nextAction'
import { services } from './services'
import { StateFn } from './state'

const createModel = (components = {}, services = {}) => (data) => {
  const seed = {
    pageId: '',
    params: {},
    url: '',
    consensus: { block: 0, time: 0 },
    accounts: {
      [UserAccount]: {},
      [DebitorAccount]: {},
      [EntitlementAccount]: {},
      [SettlementsAccount]: {},
      [IncomeAccount]: {},
      [QueriedAccount]: {},
      _lastFetch: { block: 0, time: 0 },
      _status: 'idle'
    },
    accountIds: [
      UserAccount,
      DebitorAccount,
      EntitlementAccount,
      SettlementsAccount,
      IncomeAccount,
      QueriedAccount
    ],
    creditors: {},
    creditorIds: [],
    settlement: {},
    [SettlementPage]: {},
    auth: {},
    user: {}
  }

  return mergeDeepRight(
    [ components, services ]
      .map(item => Object.values(item))
      .reduce((acc, item) => { acc = acc.concat(item); return acc }, [])
      .filter(item =>
        (item != null) &&
        (typeof item.model === 'function' || typeof item.initial === 'function')
      )
      .map(item => (item.model || item.initial))
      .map(f => f())
      .reduce(mergeDeepRight, seed),
    data
  )
}

const createView = (components) => {
  return function Root (state) {
    // const { header, footer, notFound } = components
    const page = components[state.pageId]
      ? components[state.pageId]
      : components.notFound
    // const styles = {
    //   main: { flex: '1 0 auto' },
    //   sticky: { flexShrink: '0' },
    //   wrap: {
    //     display: 'flex',
    //     flexDirection: 'column',
    //     height: '100vh'
    //   }
    // }

    // let oldReturn = (
    //   ['div', { id: state.pageId, style: styles.wrap },
    //     ['main', { style: styles.main },
    //       state.pageId !== SignInPage && header(state),
    //       page(state)
    //     ],
    //     ['div', { style: styles.sticky },
    //       footer(state)
    //     ]
    //   ]
    // )

    let newReturn = (
      [Wrapper, { id: state.pageId },
        [Main, {},
          (state.pageId !== SignInPage) && [HeaderNew, { state }],
          page(state)
        ],
        [FooterNew, { state }]
      ]
    )

    return newReturn
  }
}

export const createRoot = (update, getState, data) => {
  const actions = createActions({ furyRpc, localStore })({ update })
  const header = createHeader()
  const footer = createFooter()
  const notFound = createNotFound()
  const signIn = createSignIn({ update })
  const account = createAccount({ parentActions: actions })
  const settlement = createSettlement({ parentActions: actions, getState, update })

  const components = {
    header: header.view,
    footer: footer.view,
    notFound: notFound.view,
    [HomePage]: account.view,
    [SignInPage]: signIn.view,
    [SettlementPage]: settlement.view,

    [DebitorAccount]: account.view,
    [EntitlementAccount]: account.view,
    [IncomeAccount]: account.view,
    [QueriedAccount]: account.view,
    [SettlementsAccount]: account.view,
    [UserAccount]: account.view
  }

  return {
    nextAction: createNextAction({ localStore, furyRpc })(update),
    model: () => createModel({ signIn, settlement }, services)(data),
    state: StateFn,
    view: createView(components)
  }
}

const dependencies = {
  header: Header,
  footer: Footer,
  notFound: NotFound,
  [SignInPage]: Authentication,
  [HomePage]: Account,
  [SettlementPage]: Settlement,

  [DebitorAccount]: Account,
  [EntitlementAccount]: Account,
  [IncomeAccount]: Account,
  [QueriedAccount]: Account,
  [SettlementsAccount]: Account,
  [UserAccount]: Account
}

export const Root = {
  dependencies,
  actions: createActions({ furyRpc, localStore }), // ({ update, getState, parentActions }) -> Object
  model: createModel({ Authentication, Settlement }, services), // data -> Object
  nextAction: createNextAction({ furyRpc, localStore }), // (update) -> state -> void 0
  state: StateFn, // model -> state
  view: createView // ({ actions?, ...dependencies? }) -> state -> vNode
}
