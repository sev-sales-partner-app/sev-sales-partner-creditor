// import { p } from '../util/debug'
import {
  // asyncPipe
  pipe
} from '../util/fp'

import { services } from './services'

// // createState :: Object -> Object -> Object
// export const createStateFn = // services -> model -> state
//     services => {
//       return asyncPipe(
//         ...Object
//           .values(services)
//           .map(s => s.state)
//       )
//     }

// State :: Object -> Object
export const StateFn = // model -> state
  pipe(
    ...Object
      .values(services)
      .map(s => s.state)
  )
