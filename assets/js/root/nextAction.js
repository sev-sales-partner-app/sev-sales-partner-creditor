import { now } from '../util'
// import { HomePage } from '../util/constants'
import { p } from '../util/debug'
import { assocPath, compose } from '../util/fp'

// const updates = {
//   setNapTestData: data => assoc('nextDataTest')(data),
//   consensus: data => assoc('consensus')(data)
// }

// export const createNextAction = (update) => {
//   const loadDataNapTest = compose(update, updates.setNapTestData)
//
//   return function nextAction (state) {
//     const { pageId, nextDataTest } = state;
//
//     [[ pageId === HomePage, !nextDataTest ]] // conditions
//       .filter(conditions => conditions.every(Boolean))
//       .map(() => {
//         p('[dataLoaderTest :: nextAction]', 'test nextAction in Root')
//
//         setTimeout(() => {
//           loadDataNapTest('The data has been loaded via nextActions().')
//         }, 1234)
//       })
//   }
// }

export const createNextAction = ({ furyRpc, localStore }) => update => {
  let prevPage
  let prevCons
  const setBlock = assocPath([ 'consensus', 'block' ])
  const setTime = assocPath([ 'consensus', 'time' ])

  return function consensusLoader (state) {
    [[ prevCons == null, state.pageId !== prevPage && state.consensus.time + 19 < now() ]]
      .filter(conditions => conditions.some(Boolean))
      .map(() => {
        prevPage = state.pageId
        prevCons = state.consensus

        p('[consensusLoader :: nextAction]', 'retrieve current block number')
        furyRpc
          .fetchCurrentBlockNumber()
          .then(nblk =>
            nblk > prevCons.block
              ? localStore
                .matchBlockTime(nblk)
                .then(({ block, time }) => {
                  prevCons = { block, time }
                  p('[consensusLoader :: nextAction]', 'matched block time')
                  update(compose(
                    setBlock(block),
                    setTime(time)
                  ))
                })
              : void 0
          )
          .catch(err => console.error('[consensusLoader :: nextAction]', err))
      })
  }
}
