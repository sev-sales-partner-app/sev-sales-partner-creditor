import { b as css, createElement as el } from '../util/view'

// const { colors, fontSizes, space, radii } = theme

export function Notification (props) {
  // const styles = {}

  const className = [
    'Notification',
    props.type || 'fullscreen',
    props.className,
    // styles[props.type],
    props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })
  attrs.css && delete attrs.css

  return el('div', attrs)
}
