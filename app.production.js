const optimize = require('spike-optimize')
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  devtool: false,
  afterSpikePlugins: [...optimize({
    scopeHoisting: true,
    aggressiveSplitting: true,
    minify: true
  })],

  // see https://reactjs.org/docs/optimizing-performance.html#webpack
  plugins: [
    new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify('production') })
  ].concat(process.env.NODE_ENV === 'analyze' ? [ new BundleAnalyzerPlugin() ] : [])
}
