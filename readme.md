# SEV Sales Partner App Creditor UI

App to claim sales commission

## Preview

preview at [https://sevcreditor.netlify.com/#/](https://sevcreditor.netlify.com/#/)

##### Sign in credentials for testing
- username: `<you@your-e-mail.address>`
- Sales Partner ID: `c3ec23a16304f8d6c8692dcac2343c05`

## Setup

- make sure [node.js](http://nodejs.org) is at version >= `6`
- `npm i spike -g`
- clone this repo down and `cd` into the folder
- run `npm install`
- run `npm start` to compile and load the app into your browser
- run `npm run prod` to compile for production

Output is compiled into the `./public` folder

## Testing
Tests are located in `test/**` and are powered by [ava](https://github.com/sindresorhus/ava)
- `npm install` to ensure devDeps are installed
- `npm test` to run test suite
