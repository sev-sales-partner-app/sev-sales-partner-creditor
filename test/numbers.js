import test from 'ava'
import { $toFury, furyTo$, furyTokWh, $multiplier } from '../assets/js/util/furyNumbers'

const val1 = 10000000
const val2 = 12345678
const val3 = 98765432
const val4 = 12345678900
const val5 = 0.123456789

test('expects fury multiplyer to be 10000000', (t) => {
  t.is(val1, $multiplier, 'fury multiplier is 10000000')
})

test('converts Fury integer to €uro strings and floats', (t) => {
  t.is(
    typeof furyTo$.string(val2),
    'string',
    'furyTo$ output to be of type string'
  )
  t.is(
    furyTo$.string(val2),
    ['1', ',23', '\u00a0€'].join(''),
    'expect correct round down precision \'.2f\''
  )
  t.is(
    furyTo$.string(val3),
    ['9', ',', '88', '\u00a0€'].join(''),
    'expect correct round up precision \'.2f\''
  )
  t.is(
    furyTo$.string(val4),
    ['1', '.', '234', ',', '57', '\u00a0€'].join(''),
    'formats thousands separator for de-DE locale correctly'
  )
  t.is(
    furyTo$(val2),
    1.23,
    'has option to output to number'
  )
  t.true(
    Number(furyTo$(val2)) === furyTo$(val2) && furyTo$(val2) % 1 !== 0,
    'number output can be of type float'
  )
})

test('converts floats to Fury integer', t => {
  t.true(
    typeof $toFury(9898989) === 'number' &&
    typeof $toFury(892348.9384) === 'number' &&
    typeof $toFury('324324.324324') === 'number',
    'fury integer is number'
  )
  t.is(
    $toFury(val5),
    1234568,
    'rounds fury integer as expected'
  )
  t.is(
    $toFury((val5).toString(10)),
    1234568,
    'allows stingified numbers as arguments'
  )
  t.true(
    isNaN($toFury(NaN)) && isNaN($toFury()),
    'fails silently on invalid arguments'
  )
})

test('converts Fury Wh value to kWh', t => {
  t.is(
    furyTokWh(1000),
    1,
    'converts Wh to kWh'
  )
  t.is(
    furyTokWh.string(1000),
    ['1', '\u00a0kWh'].join(''),
    'has option to output to string'
  )
  t.is(
    furyTokWh.string(1000456),
    ['1.000,46', '\u00a0kWh'].join(''),
    'formats de-DE locale correctly'
  )
})
